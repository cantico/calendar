��    [      �     �      �     �     �  
   �     �  	   �                 
   9     D     J     N     [     g     u     �     �     �     �     �     �      	  
   	     	     !	     *	     1	  &   @	     g	  
   k	     v	  ?   �	  	   �	     �	     �	     �	     �	     �	     �	  	   
     
     
     
     -
     5
     N
     [
     d
     j
     z
     �
     �
     �
     �
  
   �
     �
     �
     �
     �
  
          +         H  $   i  *   �  &   �  !   �       7     %   W     }  
   �     �     �     �     �  >   �     �  &        .  	   3     =     D     H     O     l     q     w     ~     �  $  �     �     �     �     �     �  
   �     �        
   -  	   8     B     G     _     k     {      �     �     �  (   �  !        9     U     f     }     �     �     �  ,   �     �     �       D        Z     h      m     �     �     �     �     �     �     �     �     �     �                ,     5     J  .   ]     �     �     �     �  (   �     �          *     H     X  :   \  /   �  6   �  0   �  9   /  0   i     �  ;   �  1   �     '     +     ;     A     M  %   Q  B   w     �     �     �     �                 !   %     G     M     S     X     a         '      "   <          %      Q              V   6         I              =   2      ;   /       [   D   8                                        *   J   +      F   K   R   C   A           M   0   5       7          ?       :   N   @   9   P             G   O   &   -   !   
       $             H   B      )   #   1   S       Y         4           .                      Z             E   U               L      W   	           (   >             ,       T      3   X       Agenda order Background color Begin date Calendar Calendars Category Center buttons Configurable calendar view. Created by Daily Day Delete event Description Detailed view Display calendar list Display category name Display end Display header bar Display link to detailed view Display past events Display start Display type Edit event End date End time Events Events overlap Fixed (6 weeks, fixed calendar height) Fri Header bar Left buttons Liquid (Number of weeks linked to month, fixed calendar height) List mode Location Mask past events Mon Month Monthly New calendar New event Next Once Open detailed view Per day Per day and per calendar Portal tools Previous Print Reorder agendas Repeat every Repeat period ends on Repeats Right buttons Sat Save event Save global configuration Save my configuration Site default Slot duration Start time Sun The end date of the recurence is mandatory. The event date must not be empty The event end time must not be empty The event must include at last one minute. The event start time must not be empty The event title must not be empty The period is not available. The reservation is in conflict with other reservations. This event exists on others calendars Thu Time slots Title Today Tue Use default configuration Variable (Number of weeks and calendar height linked to month) Visible week days We have a problem finding this page... Week Week mode Weekly Wen Yearly You must select the calendar days hh:mm months weeks years Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-10-31 11:01+0100
PO-Revision-Date: 2018-10-31 11:04+0100
Last-Translator: Laurent Choulette <laurent.choulette@cantico.fr>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: calendar_translate;calendar_translate:1,2;translate:1,2;translate
X-Poedit-Basepath: ..
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: programs
 Ordre des agendas Couleur de fond Date de début Agenda Agendas Catégorie Boutons au centre Affichage d'agenda configurable. Créé par Quotidien Jour Supprimer l'événement Description Vue détaillée Afficher la liste des agendas Afficher le nom de la catégorie Heure de fin d'affichage Afficher la barre d'outils Afficher un lien vers la vue détaillée Afficher les événements passés Heure de début d'affichage Type d'affichage Modifier l'événement Date de fin Heure de fin Événements Les événements se superposent Fixe (6 semaines, taille du calendrier fixe) Ven Barre d'outils Boutons à gauche Liquide (Nombre de semaines lié au mois, taille du calendrier fixe) Mode de liste Lieu Masquer les événements passés Lun Mois Mensuel Nouvel agenda Nouvel événement Suivant Une seule fois Ouvrir la vue détaillée Par jour Par jour et par agenda Outils portail Précédent Imprimer Ordonner les agendas Répéter tous les Définir la fin de la période de répétition Répétition Boutons à droite Sam Enregistrer l'événement Sauvegarder la configuration par défaut Sauvegarder ma configuration Comme défini pour le site Durée des créneaux horaires Heure de début Dim La date de fin de période de récurrence est obligatoire. La date de l'événement ne doit pas être vide L'heure de fin de l'événement ne doit pas être vide L'événement doit contenir au moins une minute. L'heure de début de l'événement ne doit pas être vide Le titre de l'événement ne doit pas être vide La plage n'est pas disponible. La réservation est en conflit avec d'autres réservations. Cet évènement est présent sur d'autres agendas Jeu Plages horaires Titre Aujourd'hui Mar Utiliser la configuration par défaut Variable (Nombre de semaine et taille du calendrier liés au mois) Jours de la semaine visibles Impossible de trouver la page Semaine Mode de semaine Hebdomadaire Mer Annuel Vous devez sélectionner l'agenda jours hh:mm mois semaines années 