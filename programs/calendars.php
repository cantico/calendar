<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2017 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
include_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';





/**
 * @return int		The current user's personal calendar id
 */
function calendar_getPersonalCalendarId()
{
    $personalCalendar = bab_getIcalendars()->getPersonalCalendar();

    if (!$personalCalendar) {
        return null;
    }
    return $personalCalendar->getUrlIdentifier();
}

/**
 *
 * @return array
 */
function calendar_getCalendarCategories()
{
    require_once $GLOBALS['babInstallPath'].'utilit/calapi.php';

    static $eventCategories = null;
    if (!isset($eventCategories)) {
        $eventCategories = array();
        $categories = bab_calGetCategories();
        foreach ($categories as $category) {
            $eventCategories[$category['name']] = $category;
        }
    }
    return $eventCategories;
}



/**
 * An array containing calendars visible by the current user
 * indexed by calendar type and calendar identifier.
 *
 * @param string $mode	"view" or "add"
 * @return bab_EventCalendar[][]
 */
function calendar_getAvailableCalendars($mode = 'view')
{
    $availableCalendars = array('personal' => array());

    $icalendars = bab_getICalendars();

    $calendars = $icalendars->getCalendars();

    foreach ($calendars as $key => $calendar) {
        if ($mode === 'view' && $calendar->canView()
         || $mode === 'add' && $calendar->canAddEvent()) {
            $type = $calendar->getReferenceType();
            if (!isset($availableCalendars[$type])) {
            	$availableCalendars[$type] = array();
            }

            $availableCalendars[$type][$key] = $calendar;
        }
    }

    return $availableCalendars;
}


function calendar_getPortletAvailableCalendars($portletCalendars, $mode = 'view'){
    $allCalendars = calendar_getAvailableCalendars($mode);
    $availableCalendars = array();
    foreach ($allCalendars as $calendar){
        foreach ($calendar as $c){
            $availableCalendars[] = $c->getUrlIdentifier();
        }
    }
    $displayCalendars = $portletCalendars;
    //Get aonly the agendas the user is able to read
    foreach ($displayCalendars as $key => $calendar){
        if(!in_array($calendar, $availableCalendars)){
            array_splice($displayCalendars, $key, 1);
        }
    }

    return $displayCalendars;
}

/**
 * Return the user agenda order for the portlet if set, default order otherwise
 * @param int $portletId
 * @param int $userId
 * @return mixed The user agenda order
 */
function calendar_getPortletUserAgendaOrder($portletId, $userId = null){
    if($userId === null){
        $userId = bab_getUserId();
    }
    $registryUser = bab_getRegistryInstance();
    $registryUser->changeDirectory("/calendar/$portletId/$userId/");
    $userOrder = $registryUser->getValue('agendaOrder');
    if(!is_null($userOrder)){
        if(!is_array($userOrder)){
            $userOrder = explode(',', $userOrder);
        }
        sort($userOrder);
    }

    $registryGlobal = bab_getRegistryInstance();
    $registryGlobal->changeDirectory("/calendar/$portletId/0/");
    $globalOrder = $registryGlobal->getValue('agendaOrder');
    if(!is_null($globalOrder)){
        if(!is_array($globalOrder)){
            $globalOrder = explode(',', $globalOrder);
        }
        sort($globalOrder);
    }

    if($userOrder == null || $userOrder != $globalOrder){ //User never set a custom order, or the list of displayed agendas has been changed
        $registryUser->setKeyValue('agendaOrder', $registryGlobal->getValue('agendaOrder'));
        return $registryGlobal->getValue('agendaOrder');
    }
    $registryUser = bab_getRegistryInstance();
    $registryUser->changeDirectory("/calendar/$portletId/$userId/");
    return $registryUser->getValue('agendaOrder');
}

function calendar_setPortletUserAgendaOrder($portletId, $userId, $agendaOrder){
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory("/calendar/$portletId/$userId/");
    $registry->setKeyValue('agendaOrder', $agendaOrder);
}

function calendar_deletePortletUserAgendaOrder($portletId, $userId){
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory("/calendar/$portletId/$userId/");
    return $registry->removeKey('agendaOrder');
}


/**
 * Returns periods between $start and $end.
 *
 * @param bab_DateTime $start
 * @param bab_DateTime $end
 * @throws Exception
 *
 * @return bab_UserPeriods
 */
function calendar_getCalendarEvents(bab_DateTime $start, bab_DateTime $end, $calids = null)
{
    include_once $GLOBALS['babInstallPath'].'utilit/cal.userperiods.class.php';

    $userPeriods = new bab_UserPeriods($start, $end);

    /* @var $factory bab_PeriodCriteriaFactory */
    $factory = bab_getInstance('bab_PeriodCriteriaFactory');

    $criteria = $factory->Collection(
        array(
            'bab_NonWorkingDaysCollection',
            'bab_VacationPeriodCollection',
            'bab_CalendarEventCollection',
            'bab_InboxEventCollection'
        )
    );

    $calendars = array();
    foreach ($calids as $idcal) {
        $calendar = bab_getICalendars()->getEventCalendar($idcal);
        if (!$calendar) {
            continue;
            //throw new Exception('Calendar not found for identifier : (' . $idcal . ')');
        }
        $calendars[] = $calendar;
    }

    $criteria = $criteria->_AND_($factory->Calendar($calendars));

    $userPeriods->createPeriods($criteria);
    $userPeriods->orderBoundaries();

    return $userPeriods->getEventsBetween(
        $start->getTimeStamp(),
        $end->getTimeStamp(),
        null,
        false
    );
}




/**
 *
 * @param	array		$args
 *
 *	$args['startdate'] 	: array('month', 'day', 'year', 'hours', 'minutes')
 *	$args['enddate'] 	: array('month', 'day', 'year', 'hours', 'minutes')
 *	$args['owner'] 		: id of the owner
 *	$args['rrule'] 		: // BAB_CAL_RECUR_DAILY, ...
 *	$args['until'] 		: array('month', 'day', 'year')
 *	$args['rdays'] 		: repeat days array(0,1,2,3,4,5,6)
 *	$args['ndays'] 		: nb days
 *	$args['nweeks'] 	: nb weeks
 *	$args['nmonths'] 	: nb weeks
 *	$args['category'] 	: id of the category
 *	$args['private'] 	: if the event is private
 *	$args['lock'] 		: to lock the event
 *	$args['free'] 		: free event
 *	$args['alert'] 		: array('day', 'hour', 'minute', 'email'=>'Y')
 *	$args['selected_calendars']
 *
 * @param	string		&$msgerror
 * @param	string		[$action_function]
 */


/**
 * @property string	$subject		The post subject
 * @property string	$message		The post content
 */
class calendar_Event
{
	private $loaded;
	private $id = null;
	private $creatorId = null;
	private $title = null;
	private $description = null;
	private $location = null;
	private $startDatetime = null;
	private $endDatetime = null;
	private $calendarId = null;
	private $backgroundColor = null;
	private $textColor = null;
	private $category = null;
	private $allDay = false;
	private $editable = true;
	private $displayCategoryName = false;

	/**
	 */
	public function __construct($event = null)
	{
		if (is_array($event)) {
			$this->id = $event['id'];
			$this->creatorId = $event['creatorId'];
			$this->title = $event['title'];
			$this->description = $event['description'];
			$this->location = $event['location'];
			$this->startDatetime = $event['startDatetime'];
			$this->endDatetime = $event['endDatetime'];
			$this->calendarId = $event['calendarId'];
			$this->backgroundColor = $event['backgroundColor'];
			$this->textColor = '#000000';
			$this->category = $event['category'];
			$this->loaded = true;
		} else {
			$this->id = $event;
			$this->loaded = false;
		}
	}



    /**
     *
     * @param bab_CalendarPeriod $event
     * @return calendar_Event
     */
    public static function fromCalendarPeriod(bab_CalendarPeriod $event, bab_EventCalendar $calendar, $backgroundColor = 'default', $displayCategoryName = false)
    {
        $W = bab_Widgets();
        $categories = calendar_getCalendarCategories();
        include_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';

        $eventCollection = $event->getCollection();
        $calendarObj = $eventCollection->getCalendar();



        if ($calendarObj != null) {

            $calendarId = $calendarObj->getUrlIdentifier();
            $eventUid = $event->getProperty('UID');
            $evt = new calendar_Event($calendarId . ':' . $eventUid);

            $evt->category = $event->getProperty('CATEGORIES');

            $evt->displayCategoryName = $displayCategoryName;

            if ($calendarObj->canViewEventDetails($event)) {
                $evt->title = $event->getProperty('SUMMARY');
                $evt->description = $event->getProperty('DESCRIPTION');
                $evt->location = $event->getProperty('LOCATION');
            }
            $startDatetime = $event->getProperty('DTSTART');
            $evt->startDatetime = substr($startDatetime,0,4) . '-' . substr($startDatetime,4,2) . '-' . substr($startDatetime,6,2) . 'T' .  substr($startDatetime,9,2) . ':' . substr($startDatetime,11,2) . ':'  . substr($startDatetime,13,2);
            $endDatetime = $event->getProperty('DTEND');
            $evt->endDatetime = substr($endDatetime,0,4) . '-' . substr($endDatetime,4,2) . '-' . substr($endDatetime,6,2) . 'T' .  substr($endDatetime,9,2) . ':' . substr($endDatetime,11,2) . ':'  . substr($endDatetime,13,2);


            switch ($backgroundColor) {
                case 'category':
                    if (isset($categories[$evt->category])) {
                        $evt->color = '#' . $categories[$evt->category]['color'];
                    } elseif ($calendar->getBgcolor() != '') {
                        $evt->color = '#' . $calendar->getBgcolor();
                    } else {
                        $evt->color = '#EEEEEE';
                    }
                    break;
                case 'calendar':
                default:
                    if ($calendar->getBgcolor() != '') {
                        $evt->color = '#' . $calendar->getBgcolor();
                    } else {
                        $evt->color = '#EEEEEE';
                    }
                    break;
            }


            $color = $event->getProperty('X-CTO-COLOR');
            if (!empty($color)) {
                $evt->color = '#' . $color;
//             } elseif (isset($categories[$evt->category])) {
//                 $evt->color = '#' . $categories[$evt->category]['color'];
//             } elseif ($calendar->getBgcolor() != '') {
//                 $evt->color = '#' . $calendar->getBgcolor();
//             } else {
//                 $evt->color = '#EEEEEE';
            }

            $evt->editable = $calendarObj->canUpdateEvent($event);
            $evt->creatorId = $event->getAuthorId();
        } else {
            $startDatetime = $event->getProperty('DTSTART');
            $endDatetime = $event->getProperty('DTEND');

            $eventArray = array(
                'id' => null,
                'creatorId' => null,
                'title' => $event->getProperty('SUMMARY'),
                'description' => $event->getProperty('DESCRIPTION'),
                'location' => $event->getProperty('LOCATION'),
                'startDatetime' => substr($startDatetime,0,4) . '-' . substr($startDatetime,4,2) . '-' . substr($startDatetime,6,2) . 'T' .  substr($startDatetime,9,2) . ':' . substr($startDatetime,11,2) . ':'  . substr($startDatetime,13,2),
                'endDatetime' => substr($endDatetime,0,4) . '-' . substr($endDatetime,4,2) . '-' . substr($endDatetime,6,2) . 'T' .  substr($endDatetime,9,2) . ':' . substr($endDatetime,11,2) . ':'  . substr($endDatetime,13,2),
                'calendarId' => null,
                'backgroundColor' => '#FFFFFF',
                'category' => $event->getProperty('CATEGORIES')
            );

            $evt = new calendar_Event($eventArray);
            $evt->title = $event->getProperty('SUMMARY');
            $evt->description = $event->getProperty('DESCRIPTION');
            $evt->location = $event->getProperty('LOCATION');
            $evt->startDatetime = substr($startDatetime,0,4) . '-' . substr($startDatetime,4,2) . '-' . substr($startDatetime,6,2) . 'T' .  substr($startDatetime,9,2) . ':' . substr($startDatetime,11,2) . ':'  . substr($startDatetime,13,2);
            $evt->endDatetime = substr($endDatetime,0,4) . '-' . substr($endDatetime,4,2) . '-' . substr($endDatetime,6,2) . 'T' .  substr($endDatetime,9,2) . ':' . substr($endDatetime,11,2) . ':'  . substr($endDatetime,13,2);

            $evt->category = $event->getProperty('CATEGORIES');

            $evt->color = '#FFFFFF';
            $evt->creatorId = $event->getAuthorId();
            $evt->editable = !($eventCollection instanceof bab_ReadOnlyCollection);
        }

        $color = $W->Color();
        $color->setHexa($evt->color);

        list(, , $l) = $color->getHSL();

        if ($l > 0.5) {
            $evt->textColor = '#000000';
        } else {
            $evt->textColor = '#FFFFFF';
        }
        return $evt;
    }



	public function getId()
	{
		return $this->id;
	}

	public function getTitle()
	{
		$this->load();
		return $this->title;
	}


	public function getDescription()
	{
		$this->load();
		return $this->description;
	}


	public function getLocation()
	{
		$this->load();
		return $this->location;
	}


	public function getStartDatetime()
	{
		$this->load();
		return $this->startDatetime;
	}


	public function getEndDatetime()
	{
		$this->load();
		return $this->endDatetime;
	}


	public function getCreator()
	{
		$this->load();
		return $this->creatorId;
	}


	public function getCategory()
	{
		$this->load();
		return $this->category;
	}


	public function __get($propertyName)
	{
		$this->load();

		$method = 'get' . $propertyName;
		if (method_exists($this, $method)) {
			return $this->$method();
		}
	}


	public function __set($propertyName, $value)
	{
		$this->$propertyName = $value;
	}


	/**
	 * Checks whether the current user can delete this forum post.
	 *
	 * @return bool
	 */
	public function canDelete()
	{
		return true;
	}


	/**
	 * Deletes this calendar event.
	 */
	public function delete()
	{
		if (!$this->canDelete()) {
			return false;
		}
		bab_deleteEventById($this->id);
	}


	public function load()
	{
		if ($this->loaded) {
			return;
		}

		global $babDB;

		$res = $babDB->db_query('SELECT * FROM '.BAB_CAL_EVENTS_TBL.' WHERE id=' . $babDB->quote($this->id));
		if ($res && $event = $babDB->db_fetch_assoc($res)) {
			$this->creatorId = $event['creatorId'];
			$this->title = $event['title'];
			$this->description = $event['description'];
			$this->location = $event['location'];
			$this->startDatetime = $event['start_date'];
			$this->endDatetime = $event['end_date'];
			$this->category = $event['id_cat'];
//			$this->calendarId = $event['calendarId'];
			$this->backgroundColor = $event['color'];
			$this->loaded = true;
		}
		return true;
	}


    protected function getLongPeriod()
    {
        list($startDay, $startTime) = explode('T', $this->startDatetime);
        list($endDay, $endTime) = explode('T', $this->endDatetime);

        $period = substr($startTime, 0, 5) . ' - ' . substr($endTime, 0, 5);
        if ($startDay != $endDay) {
            $period .= ' (' . bab_shortDate(bab_mktime($endDay), false) . ')';
        }

        return $period;
    }

    protected function getShortPeriod()
    {
        list(, $startTime) = explode('T', $this->startDatetime);
        list(, $endTime) = explode('T', $this->endDatetime);

        $period = substr($startTime, 0, 5) . '-' . substr($endTime, 0, 5);

        return $period;
    }


    /**
     * Returns a json encoded version of the event.
     *
     * @see http://arshaw.com/fullcalendar/docs/event_data/Event_Object/
     * @return string
     */
    public function toJson()
    {
        $App = calendar_App();
        $W = bab_Widgets();

        static $ctrl = null;
        if (!isset($ctrl)) {
            $ctrl = $App->Controller();
        }

        static $canvas = null;
        if (!isset($canvas)) {
            $canvas = $W->HtmlCanvas();
        }

        $event = array(
            'id' => $this->id,
            'title' => $this->title,
            'allDay' => $this->allDay,
            'start' => str_replace(' ', 'T', $this->startDatetime),
            'end' => str_replace(' ', 'T', $this->endDatetime),
            'longPeriod' => $this->getLongPeriod(),
            'period' => $this->getShortPeriod(),
            'description' => $this->description,
            'color' => $this->color,
            'textColor' => $this->textColor,
            'location' => $this->location,
            'category' => $this->category,
            'editable' => $this->editable,
            'creatorId' => $this->creatorId
        );


        if ($this->displayCategoryName && !empty($this->category)) {
            $categoryLabel = $W->Label($this->category);
            $event['categories'] = '<span class="categories ' . Func_Icons::ICON_LEFT_SYMBOLIC . '">';
            $event['categories'] .= $categoryLabel->display($canvas);
            $event['categories'] .= '</span>';
        }
        if ($this->editable) {
            $event['actions'] = '<span class="actions ' . Func_Icons::ICON_LEFT_SYMBOLIC . '">';

            $link = $W->Link('', $ctrl->Calendar()->editEvent($this->id));
            $link->setTitle(calendar_translate('Edit event'));
            $link->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD);
            $link->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT);

            $event['actions'] .= $link->display($canvas);
            $event['actions'] .= '</span>';
        }
        if($this->creatorId){
            $event['tooltipProperties'] = '<div class="widget-strong">'.calendar_translate('Created by').' '.bab_getUserName($this->creatorId).'</div>';
        }

        return bab_json_encode($event);
    }
}
