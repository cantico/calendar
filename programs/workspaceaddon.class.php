<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/functions.php';




/**
 *
 */
class Func_WorkspaceAddon_Calendar extends Func_WorkspaceAddon implements workspace_Configurable
{

    /**
     * @return string
     */
    public function getName()
    {
        $App = workspace_App();

        return $App->translate('Calendars');
    }


    /**
     * {@inheritDoc}
     * @see Func_WorkspaceAddon::getIconClassName()
     */
    public function getIconClassName()
    {
        return Func_Icons::APPS_CALENDAR;
    }


    /**
     * @param int $workspaceId
     * @return Widget_Form
     */
    public function getConfigurationEditor($workspaceId)
    {
        $App = workspace_App();
        $W = bab_Widgets();

        $editor = parent::getConfigurationEditor($workspaceId);

        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Workspace managers can add calendars'),
                $W->CheckBox(),
                'allowAddCalendar'
            ),
            0
        );

        return $editor;
    }


    /**
     * {@inheritDoc}
     * @see workspace_Configurable::applyConfiguration()
     */
    public function applyConfiguration($workspaceId)
    {
        /* @var $workspaceApi Func_App_Workspace */
        $workspaceApi = bab_Functionality::get('App/Workspace');

        if (!$workspaceApi) {
            return;
        }

        $workspaceSet = $workspaceApi->WorkspaceSet();
        $workspace = $workspaceSet->request($workspaceSet->id->is($workspaceId));

        if ($workspace->delegation == 0 || $workspace->name == '') {
            return;
        }

        $configuration = $this->getConfiguration($workspace->id);

        $this->initializeCalendar($workspace, $configuration);
    }



    /**
     *  Creates a public calendar and sets all access rights to users of groups.
     *
     * @param workspace_Workspace   $workspace
     * @param array                 $configuration
     * @throws Exception
     * @return int                  Id of the created folder
     */
    private function initializeCalendar($workspace, $configuration)
    {
        global $babDB;

        $sql = 'SELECT * FROM '.BAB_CAL_PUBLIC_TBL.' WHERE id_dgowner=' . $babDB->quote($workspace->delegation). ' AND name=' . $babDB->quote($workspace->name);
        $res = $babDB->db_query($sql);

        if ($babDB->db_num_rows($res) > 0) {
            return;
        }

        $sql = 'INSERT INTO ' . BAB_CAL_PUBLIC_TBL . ' (name, description, id_dgowner, idsa) VALUES (' . $babDB->quote($workspace->name) . ', ' . $babDB->quote($workspace->description) . ', ' . $babDB->quote($workspace->delegation) . ', ' . $babDB->quote(0) . ')';
        $babDB->db_query($sql);

        $ownerId = $babDB->db_insert_id();
        $sql = 'INSERT INTO ' . BAB_CALENDAR_TBL . ' (owner, type) VALUES (' . $babDB->quote($ownerId) . ', ' . BAB_CAL_PUB_TYPE . ')';
        $babDB->db_query($sql);
        $calendarId = $babDB->db_insert_id();

        $readersRightsTables = array(
            BAB_CAL_PUB_VIEW_GROUPS_TBL,
            BAB_CAL_PUB_NOT_GROUPS_TBL
        );
        $writersRightsTables = array(
            BAB_CAL_PUB_MAN_GROUPS_TBL,
            BAB_CAL_PUB_VIEW_GROUPS_TBL,
            BAB_CAL_PUB_NOT_GROUPS_TBL
        );
        $administratorsRightsTables = array(
            BAB_CAL_PUB_MAN_GROUPS_TBL,
            BAB_CAL_PUB_VIEW_GROUPS_TBL,
            BAB_CAL_PUB_NOT_GROUPS_TBL
        );

        $readersGroupId = $workspace->getDelegationGroupId();
        $writersGroupId = $workspace->getWritersGroupId();
        $administratorsGroupId = $workspace->getAdministratorsGroupId();

        foreach ($readersRightsTables as $rightsTable) {
            aclAdd($rightsTable, $readersGroupId, $calendarId);
        }
        foreach ($writersRightsTables as $rightsTable) {
            aclAdd($rightsTable, $writersGroupId, $calendarId);
        }
        foreach ($administratorsRightsTables as $rightsTable) {
            aclAdd($rightsTable, $administratorsGroupId, $calendarId);
        }

//        workspace_setWorkspaceCalendarNotifications($notifyCalendar, $workspace->delegation);

        return $calendarId;
    }


    /**
     * {@inheritDoc}
     * @see Func_WorkspaceAddon::getPortletDefinitionId()
     */
    public function getPortletDefinitionId()
    {
        /* @var $portletFunc Func_PortletBackend_Calendar */
        $portletFunc = bab_Functionality::get('PortletBackend/Calendar');
        if($portletFunc){
            /* @var $portlet calendar_PortletDefinition_Calendar */
            $portlet = $portletFunc->portlet_Calendar();
            return $portlet->getId();
        }
        return false;
    }
}
