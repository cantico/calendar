<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/calendars.php';
bab_Widgets()->includePhpClass('Widget_Frame');


/**
 * An a empty form with a button box.
 */
class calendar_BaseForm extends Widget_Frame
{

    private $outerLayout = null;
    private $innerLayout = null;
    private $buttonRow = null;

    public function __construct($id)
    {
        parent::__construct($id);

        $W = bab_Widgets();

        $this->outerLayout = $W->VBoxLayout()
            ->addClass('calendar-form-layout')
            ->setVerticalSpacing(2, 'em');
        $this->setLayout($this->outerLayout);

        $this->innerLayout = $W->VBoxLayout()
        ->setVerticalSpacing(1, 'em');

        $this->outerLayout->addItem($this->innerLayout);

        $this->buttonRow = $W->HBoxLayout()
            ->setHorizontalSpacing(1, 'em')
            ->addClass('calendar-form-button-row');

        $this->outerLayout->addItem($this->buttonRow);
    }


    /**
     * Adds an item to the top part of the form.
     *
     * @param Widget_Displayable_Interface	$item
     * @return $this
     */
    public function addInnerItem(Widget_Displayable_Interface $item = null)
    {
        $this->innerLayout->addItem($item);
        return $this;
    }


    /**
     * Adds a button to button box of the form.
     *
     * @return $this
     */
    public function addButton(Widget_Displayable_Interface $button)
    {
        $this->buttonRow->addItem($button);
        return $this;
    }
}


/**
 *
 * @param string	$recipients		A string of comma-separated email addresses.
 *
 * @param bool $iCreation		The form is displayed for creating a new event (true) or editing one (false).
 * @return Widget_Form
 */
function calendar_eventEditor($calendars, $isCreation)
{
    require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';
    $W = bab_Widgets();

    $App = calendar_App();
    $ctrl = $App->Controller();

    $form = $W->Form();
    $frame = new calendar_BaseForm('event_editor');
    $frame->setName('event');

    $titleFormItem = $W->LabelledWidget(
        calendar_translate('Title'),
        $W->LineEdit()
            ->addClass('widget-100pc')
            ->setMandatory(true, calendar_translate('The event title must not be empty')),
        'title'
    );

    $locationFormItem = $W->LabelledWidget(
        calendar_translate('Location'),
        $W->LineEdit(),
        'location'
    );
    $startdateFormItem = $W->LabelledWidget(
        calendar_translate('Begin date'),
        $W->DatePicker()
            ->setMandatory(true, calendar_translate('The event date must not be empty')),
        'startdate'
    );
    $starttimeFormItem = $W->LabelledWidget(
        calendar_translate('Start time'),
        $W->TimeEdit()
            ->setMandatory(true, calendar_translate('The event start time must not be empty')),
        'starttime'
    );
    $endtimeFormItem = $W->LabelledWidget(
        calendar_translate('End time'),
        $W->TimeEdit()
            ->setMandatory(true, calendar_translate('The event end time must not be empty')),
        'endtime'
    );
    $enddateFormItem = $W->LabelledWidget(
        calendar_translate('End date'),
        $W->DatePicker(),
        'enddate'
    );

    require_once dirname(__FILE__) . '/rule.ui.php';
    $ruleItem = new calendar_RuleEditor();

//     $repeatItem =  $W->LabelledWidget(
//         calendar_translate('Repeats'),
//         $W->Select()
//             ->setName('repeat')
//             ->setCheckedValue('1')->setAssociatedDisplayable($ruleItem, array('1'))
//    );


    $bodyEditor = $W->BabHtmlEdit('bab_post_body');
    $bodyEditor->addClass('widget-100pc');
    $bodyEditor->setSizePolicy('widget-100pc');

    $descriptionFormItem = $W->LabelledWidget(
        calendar_translate('Description'),
        $bodyEditor,
        'description'
    );

    $categorySelect = $W->Select()
        ->addOption('', '');
    $categories = bab_calGetCategories();
    foreach ($categories as $category) {
        $categorySelect->addOption($category['id'], $category['name']);
    }
    $categoryFormItem = $W->LabelledWidget(
        calendar_translate('Category'),
        $categorySelect,
        'category'
    );


    $calendarSelect = $W->Select()
        ->addOption('', '');
    foreach ($calendars as $calendarId => $calendarName) {
        $calendarSelect->addOption($calendarId, $calendarName);
    }
    $calendarFormItem = $W->LabelledWidget(
        calendar_translate('Calendar'),
        $calendarSelect
            ->setMandatory(true, calendar_translate('You must select the calendar')),
        'calendar'
    );

    $frame->addInnerItem(
        $calendarFormItem->setSizePolicy('widget-100pc')
    );


    $frame->addInnerItem(
        $titleFormItem->setSizePolicy('widget-100pc')
    );


    $frame->addInnerItem(
        $W->HBoxItems(
            $startdateFormItem,
            $starttimeFormItem,
            $endtimeFormItem,
            $enddateFormItem
        )->setHorizontalSpacing(1, 'em')
    );
    $frame->addInnerItem(
        $ruleItem
    );
    $frame->addInnerItem(
        $W->FlowItems(
            $locationFormItem,
            $categoryFormItem
        )->setHorizontalSpacing(1, 'em')
    );
    $frame->addInnerItem($descriptionFormItem);



    $frame->addButton(
        $W->SubmitButton()
            ->validate(true)
            ->setLabel(calendar_translate('Save event'))
            ->setAction($ctrl->Calendar()->saveEvent())
            ->setAjaxAction()

    );

    if (!$isCreation) {
        $frame->addButton(
            $W->SubmitButton()
                ->validate(false)
                ->setLabel(calendar_translate('Delete event'))
                ->setAction($ctrl->Calendar()->deleteEvent())
        );
    }

    $form->setLayout($W->VBoxLayout())->addItem($frame);
    $form->setHiddenValue('tg', $ctrl->controllerTg);

    return $form;
}

/**
 *
 */
function calendar_eventListPerDayPerCalendar($calendars, $popup = false, $maskPastEvents = false)
{
    $W = bab_Widgets();
    $App = calendar_App();
    $ctrl = $App->Controller();
    $eventsPerDayPerCalendar = array();
    $calendarNames = array();
    $calendarColors = array();
    $calendarLinks = array();
    //$eventsPerDayPerCalendar[YYYY-MM-DD][Calendar_urlid][Event]
    $from = bab_DateTime::fromIsoDateTime(date('Y-m-d') . ' 00:00:00');
    $to = $from->cloneDate();
    $to->add(1, BAB_DATETIME_DAY);

    if($popup && isset($_SESSION['calendar']['fromDate']) && isset($_SESSION['calendar']['toDate'])){
        if(!$maskPastEvents){
            $from = bab_DateTime::fromIsoDateTime($_SESSION['calendar']['fromDate']);
        }
        $to = bab_DateTime::fromIsoDateTime($_SESSION['calendar']['toDate']);
    }

    $calendarEvents = calendar_getCalendarEvents($from, $to, calendar_getPortletAvailableCalendars($calendars));
    foreach($calendarEvents as $e){
        if(!is_null($e->getCalendars())){
            $eventCalendars = $e->getCalendars();
            foreach ($eventCalendars as $eventCalendar){
                if($e->ts_begin != $e->ts_end){
                    $eBegin = BAB_DateTime::fromTimeStamp($e->ts_begin);
                    $eEnd = BAB_DateTime::fromTimeStamp($e->ts_end);
                    $diffDays = BAB_DateTime::dateDiff(
                        $eBegin->getDayOfMonth(),
                        $eBegin->getMonth(),
                        $eBegin->getYear(),
                        $eEnd->getDayOfMonth(),
                        $eEnd->getMonth(),
                        $eEnd->getYear()
                    );
                    for($i = 0; $i<=$diffDays; $i++){
                        $eventsPerDayPerCalendar[$eBegin->getIsoDate()][$eventCalendar->getUrlIdentifier()][] = $e;
                        $eBegin->add(1, BAB_DATETIME_DAY);
                    }
                }
                else{
                    $eventsPerDayPerCalendar[BAB_DateTime::fromTimeStamp($e->ts_begin)->getIsoDate()][$eventCalendar->getUrlIdentifier()][] = $e;
                }
                $calendarNames[$eventCalendar->getUrlIdentifier()] = $eventCalendar->getName();
                $calendarColors[$eventCalendar->getUrlIdentifier()] = $eventCalendar->getBgcolor();
                $calendarLinks[$eventCalendar->getUrlIdentifier()] = $eventCalendar->getUrlIdentifier().':'.$e->getProperty('UID');
            }
        }
    }
//     var_dump($calendars);
//     var_dump($eventsPerDayPerCalendar);
    foreach ($eventsPerDayPerCalendar as $date => $events){
        $allEvents = array_replace(array_flip($calendars), $events);
        $eventsPerDayPerCalendar[$date] = $allEvents;
    }
//     var_dump($eventsPerDayPerCalendar);
    $listEventsBox = $W->VBoxItems()->setHorizontalSpacing(0.5, 'em');
    foreach($eventsPerDayPerCalendar as $date => $calendarEvents){
        $currentDateBox = $W->VBoxItems()->setSizePolicy('calendar-event-box');
        $currentDateBox->addItem(
            $W->Title(BAB_DateTime::fromIsoDateTime($date)->longFormat(false), 2)
        );
        foreach($calendarEvents as $calendar => $events){
            if(is_array($events)){
                $currentCalendarBox = $W->HBoxItems()->setSizePolicy('calendar-event-agenda');
                $currentCalendarBox->addItem(
                    $W->Label($calendarNames[$calendar])
                        ->addClass('widget-counter-label')
                        ->setCanvasOptions(Widget_Item::options()->backgroundColor('#' . $calendarColors[$calendar]))
                        ->setSizePolicy('calendar-agenda-name')
                );
                $currentEventsBox = $W->VBoxItems();
                foreach ($events as $event){
                    $eventStartTimeStamp = BAB_DateTime::fromTimeStamp($event->ts_begin);
                    $eventHours = $eventStartTimeStamp->getHour().'h';
                    if($eventStartTimeStamp->getMinute() != 0){
                        $eventHours.=$eventStartTimeStamp->getMinute();
                    }
                    $eventEndTimeStamp = BAB_DateTime::fromTimeStamp($event->ts_end);
                    $eventHours .= ' - '.$eventEndTimeStamp->getHour().'h';
                    if($eventEndTimeStamp->getMinute() != 0){
                        $eventHours.=$eventEndTimeStamp->getMinute();
                    }
                    if($event->getProperty('CATEGORIES')){
                        $eventHours.=' ('.$event->getProperty('CATEGORIES').')';
                    }
                    $eventDescription = '';
                    if($event->getProperty('DESCRIPTION')){
                        $eventDescription = calendar_translate('Description').' : '.$event->getProperty('DESCRIPTION');
                    }
                    $eventLocation ='';
                    if($event->getProperty('LOCATION')){
                        $eventDescription = calendar_translate('Location').' : '.$event->getProperty('LOCATION');
                    }

                    $titleBox = $W->FlowItems();
                    $titleBox->addItem(
                        $W->Label($eventHours)->setSizePolicy('calendar-event-hours')
                        );
                    if($event->getCollection()->getCalendar()->canUpdateEvent($event)){
                        $link = $W->Link('', $ctrl->Calendar()->editEvent($calendar.':'.$event->getProperty('UID')));
                        $link->setTitle(calendar_translate('Edit event'));
                        $link->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD);
                        $link->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT);
                        $link->addClass('edit-event');
                        $titleBox->addItem($link);
                        $titleBox->setSizePolicy(Func_Icons::ICON_LEFT_16);
                    }

                    $currentEventsBox->addItems(
                        $titleBox,
                        $W->VBoxItems(
                            $W->Label(calendar_translate('Title').' : '.$event->getProperty('SUMMARY')),
                            $W->Label($eventDescription),
                            $W->Label($eventLocation)
                        )->setSizePolicy('calendar-event-details')
                    );
                }
                $currentCalendarBox->addItem($currentEventsBox);
                $currentDateBox->addItem($currentCalendarBox);
            }
        }
        $listEventsBox->addItem($currentDateBox);
    }
    return $listEventsBox;
}

function calendar_eventListPerDay($calendars, $popup = false, $maskPastEvents = false)
{
    $W = bab_Widgets();
    $App = calendar_App();
    $ctrl = $App->Controller();
    $eventsPerDay = array();
    //$eventsPerDay[YYYY-MM-DD][Event]

    $from = bab_DateTime::fromIsoDateTime(date('Y-m-d') . ' 00:00:00');
    $to = $from->cloneDate();
    $to->add(1, BAB_DATETIME_DAY);

    if($popup && isset($_SESSION['calendar']['fromDate']) && isset($_SESSION['calendar']['toDate'])){
        if(!$maskPastEvents){
            $from = bab_DateTime::fromIsoDateTime($_SESSION['calendar']['fromDate']);
        }
        $to = bab_DateTime::fromIsoDateTime($_SESSION['calendar']['toDate']);
    }
    $calendarEvents = calendar_getCalendarEvents($from, $to, calendar_getPortletAvailableCalendars($calendars));
    foreach($calendarEvents as $e){
        if(!is_null($e->getCollection()->getCalendar())){
            if($e->ts_begin != $e->ts_end){
                $eBegin = BAB_DateTime::fromTimeStamp($e->ts_begin);
                $eEnd = BAB_DateTime::fromTimeStamp($e->ts_end);
                $diffDays = BAB_DateTime::dateDiff(
                    $eBegin->getDayOfMonth(),
                    $eBegin->getMonth(),
                    $eBegin->getYear(),
                    $eEnd->getDayOfMonth(),
                    $eEnd->getMonth(),
                    $eEnd->getYear()
                    );
                for($i = 0; $i<=$diffDays; $i++){
                    $eventsPerDay[$eBegin->getIsoDate()][] = $e;
                    $eBegin->add(1, BAB_DATETIME_DAY);
                }
            }
            else{
                $eventsPerDay[BAB_DateTime::fromTimeStamp($e->ts_begin)->getIsoDate()][] = $e;
            }
        }
    }

    $listEventsBox = $W->VBoxItems()->setHorizontalSpacing(0.5, 'em');
    foreach($eventsPerDay as $date => $events){
        $currentDateBox = $W->VBoxItems()->setSizePolicy('calendar-event-box');
        $currentDateBox->addItem(
            $W->Title(BAB_DateTime::fromIsoDateTime($date)->longFormat(false), 2)
            );
        foreach($events as $event){
            $currentEventBox = $W->HBoxItems()->setSizePolicy('calendar-event-agenda');
            $eventStartTimeStamp = BAB_DateTime::fromTimeStamp($event->ts_begin);
            $eventHours = $eventStartTimeStamp->getHour().'h';
            if($eventStartTimeStamp->getMinute() != 0){
                $eventHours.=$eventStartTimeStamp->getMinute();
            }

            $eventEndTimeStamp = BAB_DateTime::fromTimeStamp($event->ts_end);
            $eventHours .= ' - '.$eventEndTimeStamp->getHour().'h';
            if($eventEndTimeStamp->getMinute() != 0){
                $eventHours.=$eventEndTimeStamp->getMinute();
            }
            if($event->getProperty('CATEGORIES')){
                $eventHours.=' ('.$event->getProperty('CATEGORIES').')';
            }
            $eventDescription = '';
            if($event->getProperty('DESCRIPTION')){
                $eventDescription = calendar_translate('Description').' : '.$event->getProperty('DESCRIPTION');
            }
            $eventLocation ='';
            if($event->getProperty('LOCATION')){
                $eventLocation = calendar_translate('Location').' : '.$event->getProperty('LOCATION');
            }


            $titleBox = $W->FlowItems();
            $titleBox->addItem(
                $W->Label($eventHours)->setSizePolicy('calendar-event-hours')
            );
            if($event->getCollection()->getCalendar()->canUpdateEvent($event)){
                $link = $W->Link('', $ctrl->Calendar()->editEvent($event->getCollection()->getCalendar()->getUrlIdentifier().':'.$event->getProperty('UID')));
                $link->setTitle(calendar_translate('Edit event'));
                $link->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD);
                $link->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT);
                $link->addClass('edit-event');
                $titleBox->addItem($link);
                $titleBox->setSizePolicy(Func_Icons::ICON_LEFT_16);
            }

            $calendarNameBox = $W->VBoxItems();
            $eventCalendars = $event->getCalendars();
            if(count($eventCalendars) > 1){
                $calendarNameBox->setSizePolicy('linked-calendars-event');
            }
            foreach ($eventCalendars as $eventCalendar){
                $calendarNameBox->addItem(
                    $W->Label($eventCalendar->getName())
                    ->addClass('widget-counter-label event-calendar-name')
                    ->setCanvasOptions(Widget_Item::options()->backgroundColor('#' . $eventCalendar->getBgColor()))
                );
            }

            $currentEventBox->addItems(
                $calendarNameBox,
                $W->VBoxItems(
                    $titleBox,
                    $W->VBoxItems(
                        $W->Label(calendar_translate('Title').' : '.$event->getProperty('SUMMARY')),
                        $W->Label($eventDescription),
                        $W->Label($eventLocation)
                    )->setSizePolicy('calendar-event-details')
                )

            ->setSizePolicy('calendar-agenda-name')
            );
            $currentDateBox->addItem($currentEventBox);
        }
        $listEventsBox->addItem($currentDateBox);
    }
    return $listEventsBox;
}

function calendar_eventListCustom($portletId, $calendars)
{
    $calendars = explode(',', $calendars);
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory("/calendar/$portletId/0/");
    if($registry->getValue('agendaOrder') === null){
        $registry->setKeyValue('agendaOrder', $calendars);
    }
    $userOrder = calendar_getPortletUserAgendaOrder($portletId, bab_getUserId());
    //var_dump($userOrder);
}

function calendar_editAgendaListOrder($portletId, $calendars)
{
    $W = bab_Widgets();
    $page = $W->babPage();
    $App = calendar_App();
    $ctrl = $App->Controller();

    $form = $W->Form();
    $form->setHiddenValue('tg', bab_rp('tg'));
    $form->setHiddenValue('portletId', $portletId);

    $formItemsBox = $W->VBoxLayout()->setVerticalSpacing(3, 'em');

    $formItemsBox->addItem(
        $W->HBoxItems(
            $W->Title(calendar_translate('Agenda order'), 2),
            $W->SubmitButton()
                ->setAction($ctrl->Calendar()->useDefaultAgendaListOrder())
                ->setAjaxAction()
                ->setLabel(calendar_translate('Use default configuration')
            )->addClass('calendar-usedefault-button')
        )->addClass('calendar-order-editor-titlebox')
    );

    $userAgendaOrder = calendar_getPortletUserAgendaOrder($portletId, bab_getUserId());
    if(!is_array($userAgendaOrder)){
        $userAgendaOrder = explode(',', $userAgendaOrder);
    }
    $allCalendars = array();
    foreach ($userAgendaOrder as $idcal){
        $calendar = bab_getICalendars()->getEventCalendar($idcal);
        if (!$calendar) {
            throw new Exception('Calendar not found for identifier : (' . $idcal . ')');
        }
        $allCalendars[] = $calendar;
    }

    $calendarSortBox = $W->VBoxLayout()->sortable(true);
    foreach ($allCalendars as $key => $c){
        $calendarSortBox->addItem(
            $W->FlowItems(
                $W->Hidden()->setName('agendaOrder[]')->setValue($c->getUrlIdentifier()),
                $W->Label($c->getName())
                    ->addClass('widget-counter-label')
                    ->setCanvasOptions(Widget_Item::options()->backgroundColor('#' . $c->getBgcolor()))
            )
        );
    }

    $formItemsBox->addItem($calendarSortBox);
    $submitBox = $W->HBoxLayout()->setHorizontalSpacing(8, 'em');
    $submitBox->addItem(
        $W->SubmitButton()
            ->setAction($ctrl->Calendar()->saveAgendaListOrder())
            ->setAjaxAction()
            ->setLabel(calendar_translate('Save my configuration'))
    );
    if(bab_isUserAdministrator()){
        $submitBox->addItem(
            $W->SubmitButton()
                ->setAction($ctrl->Calendar()->saveGlobalAgendaListOrder())
                ->setAjaxAction()
                ->setLabel(calendar_translate('Save global configuration'))
        );
    }
    $formItemsBox->addItem($submitBox);
    $form->addItem($formItemsBox);

    $page->addItem($form);
    return $page;
}

