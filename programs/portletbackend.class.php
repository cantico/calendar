<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2017 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';



require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/calendars.php';
require_once dirname(__FILE__) . '/controller.class.php';

bab_Functionality::includefile('PortletBackend');


$W = bab_Widgets();
$W->includePhpClass('Widget_FullCalendar');


$GLOBALS['Func_PortletBackend_Calendar_Categories'] = array(
    'portal_tools' => calendar_translate('Portal tools')
);


/**
 * Crm Portlet backend
 */
class Func_PortletBackend_Calendar extends Func_PortletBackend
{

    /**
     * @return string
     */
    public function getDescription()
    {
        return calendar_translate('Calendar');
    }


    /**
     * @param string $category
     * @return calendar_PortletDefinition_Calendar[]
     */
    public function select($category = null)
    {
        $addon = bab_getAddonInfosInstance('calendar');
        if (!$addon || !$addon->isAccessValid()) {
            return array();
        }

        global $Func_PortletBackend_Calendar_Categories;
        if (empty($category) || in_array($category, $Func_PortletBackend_Calendar_Categories)) {
            return array(
                'Calendar' => $this->portlet_Calendar(),
            );
        }

        return array();
    }


    /**
     * @return calendar_PortletDefinition_Calendar
     */
    public function portlet_Calendar()
    {
        return new calendar_PortletDefinition_Calendar();
    }


    /**
     * get a list of categories supported by the backend
     * @return Array
     */
    public function getCategories()
    {
        global $Func_PortletBackend_Calendar_Categories;

        return $Func_PortletBackend_Calendar_Categories;
    }

    /**
     * (non-PHPdoc)
     * @see Func_PortletBackend::getConfigurationActions()
     */
    public function getConfigurationActions()
    {
        return array();
    }
}







////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////


class calendar_PortletDefinition_Calendar implements portlet_PortletDefinitionInterface
{
    /**
     * @var bab_addonInfos $addon
     */
    protected $addon;

    /**
     *
     */
    public function __construct()
    {
        $this->addon = bab_getAddonInfosInstance('calendar');
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getId()
     */
    public function getId()
    {
        return 'Calendar';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getName()
     */
    public function getName()
    {
        return calendar_translate('Calendar');
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getDescription()
     */
    public function getDescription()
    {
        return calendar_translate('Configurable calendar view.');
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getPortlet()
     */
    public function getPortlet()
    {
        return new calendar_Portlet_Calendar();
    }


    /**
     * @return array
     */
    public function getPreferenceFields()
    {
        require_once dirname(__FILE__) . '/calendar.ctrl.php';
        $W = bab_Widgets();

        $availableCalendars = calendar_getAvailableCalendars();

        $calendarSelect = $W->Multiselect();
        $calendarSelect->setSelectedList(7);
        $calendarSelect->addClass('widget-40em');

        foreach ($availableCalendars as $calendars) {
            if (count($calendars) > 0) {
                $options = array();
                foreach ($calendars as $calendarId => $calendar) {
                    $options[$calendarId] = $calendar->getName();
                }
                $type = $calendar->getType();
                bab_Sort::natsort($options);
                $calendarSelect->addOptions($options, $type);
            }
        }

        $buttonOptions = array(
            Widget_FullCalendar::TITLE =>               calendar_translate('Title'),
            Widget_FullCalendar::VIEW_MONTH =>          calendar_translate('Month'),
            Widget_FullCalendar::VIEW_WEEK =>           calendar_translate('Week'),
            Widget_FullCalendar::VIEW_DAY =>            calendar_translate('Day'),
            Widget_FullCalendar::BUTTON_PREVIOUS =>     calendar_translate('Previous'),
            Widget_FullCalendar::BUTTON_TODAY =>        calendar_translate('Today'),
            Widget_FullCalendar::BUTTON_NEXT =>         calendar_translate('Next'),
        );

        $leftButtonsSelect = $W->Multiselect();
        $leftButtonsSelect->setSelectedList(7);
        $leftButtonsSelect->addClass('widget-100pc');
        $leftButtonsSelect->addOptions($buttonOptions);

        $centerButtonsSelect = $W->Multiselect();
        $centerButtonsSelect->setSelectedList(7);
        $centerButtonsSelect->addClass('widget-100pc');
        $centerButtonsSelect->addOptions($buttonOptions);

        $rightButtonsSelect = $W->Multiselect();
        $rightButtonsSelect->setSelectedList(7);
        $rightButtonsSelect->addClass('widget-100pc');
        $rightButtonsSelect->addOptions($buttonOptions);


        $daysBox = $W->FlowItems();
        $daysBox->setHorizontalSpacing(1, 'em');
        $dayNames = bab_DateStrings::getDays();
        $sunday = $dayNames[0];
        unset($dayNames[0]);
        $dayNames[0] = $sunday;
        foreach ($dayNames as $dayNumber => $dayName) {
            $daysBox->addItem(
                $W->LabelledWidget(
                    mb_substr($dayName, 0 , 3),
                    $W->CheckBox()
                        ->setValue(true),
                    array('visibleDays', (string)$dayNumber)
                )
            );
        }



        return array(
            array(
                'type' => 'widget',
                'item' => $W->LabelledWidget(
                    calendar_translate('Calendars'),
                    $calendarSelect,
                    'calendars'
                )
            ),
            array(
                'type' => 'list',
                'name' => 'displayType',
                'label' => calendar_translate('Display type'),
                'options' => array(
                    array(
                        'label' => calendar_translate('Month'),
                        'value' => Widget_FullCalendar::VIEW_MONTH
                    ),
                    array(
                        'label' => calendar_translate('Week'),
                        'value' => Widget_FullCalendar::VIEW_WEEK
                    ),
                    array(
                        'label' => calendar_translate('Day'),
                        'value' => Widget_FullCalendar::VIEW_DAY
                    ),
                    array(
                        'label' => calendar_translate('Detailed view'),
                        'value' => calendar_CtrlCalendar::TYPE_LIST
                    )
                )
            ),
            array(
                'type' => 'list',
                'name' => 'displayMode',
                'label' => calendar_translate('List mode'),
                'options' => array(
                    array(
                        'label' => calendar_translate('Per day and per calendar'),
                        'value' => calendar_CtrlCalendar::LIST_MODE_DATE_CALENDAR
                    ),
                    array(
                        'label' => calendar_translate('Per day'),
                        'value' => calendar_CtrlCalendar::LIST_MODE_DATE
                    )
                )
            ),
            array(
                'type' => 'list',
                'name' => 'weekMode',
                'label' => calendar_translate('Week mode'),
                'options' => array(
                    array(
                        'label' => calendar_translate('Fixed (6 weeks, fixed calendar height)'),
                        'value' => 'fixed'
                    ),
                    array(
                        'label' => calendar_translate('Liquid (Number of weeks linked to month, fixed calendar height)'),
                        'value' => 'liquid'
                    ),
                    array(
                        'label' => calendar_translate('Variable (Number of weeks and calendar height linked to month)'),
                        'value' => 'variable'
                    )
                )
            ),
            array(
                'type' => 'widget',
                'item' => $W->Section(
                    calendar_translate('Header bar'),
                    $W->VBoxItems(
                        $W->LabelledWidget(
                            calendar_translate('Display header bar'),
                            $W->CheckBox(),
                            'displayHeaderBar'
                        ),
                        $W->LabelledWidget(
                            calendar_translate('Display calendar list'),
                            $W->CheckBox(),
                            'displayCalendars'
                        ),
                        $W->LabelledWidget(
                            calendar_translate('Display link to detailed view'),
                            $W->CheckBox(),
                            'displayDetailedViewLink'
                        ),
                        $W->FlowItems(
                            $W->LabelledWidget(
                                calendar_translate('Left buttons'),
                                $leftButtonsSelect,
                                'leftButtons'
                            )->setSizePolicy('widget-33pc'),
                            $W->LabelledWidget(
                                calendar_translate('Center buttons'),
                                $centerButtonsSelect,
                                'centerButtons'
                            )->setSizePolicy('widget-33pc'),
                            $W->LabelledWidget(
                                calendar_translate('Right buttons'),
                                $rightButtonsSelect,
                                'rightButtons'
                            )->setSizePolicy('widget-33pc')
                        )->setHorizontalSpacing(1, 'em')
                        ->setVerticalAlign('top')
                    )->setVerticalSpacing(1, 'em')
                )->setFoldable(true, true)
            ),
            array(
                'type' => 'widget',
                'item' => $W->Section(
                    calendar_translate('Visible week days'),
                    $daysBox
                )->setFoldable(true, true)
            ),
            array(
                'type' => 'widget',
                'item' => $W->Section(
                    calendar_translate('Time slots'),
                    $W->VBoxItems(
                        $W->FlowItems(
                            $W->LabelledWidget(
                                calendar_translate('Display start'),
                                $W->TimeEdit()
                                    ->setValue('00:00')
                                    ->setPlaceHolder(calendar_translate('hh:mm')),
                                'minTime'
                            ),
                            $W->LabelledWidget(
                                calendar_translate('Display end'),
                                $W->TimeEdit()
                                    ->setValue('23:59')
                                    ->setPlaceHolder(calendar_translate('hh:mm')),
                                'maxTime'
                            )
                        )->setHorizontalSpacing(2, 'em'),
                        $W->LabelledWidget(
                            calendar_translate('Slot duration'),
                            $W->TimeEdit()
                                ->setValue('00:30')
                                ->setPlaceHolder(calendar_translate('hh:mm')),
                            'slotDuration'
                        ),
                        $W->LabelledWidget(
                            calendar_translate('Events overlap'),
                            $W->CheckBox(),
                            'slotEventOverlap'
                        )
                    )->setVerticalSpacing(1, 'em')
                )->setFoldable(true, true)
            ),
            array(
                'type' => 'widget',
                'item' => $W->Section(
                    calendar_translate('Events'),
                    $W->VBoxItems(
                        $W->LabelledWidget(
                            calendar_translate('Display category name'),
                            $W->CheckBox()
                                ->setValue(false),
                            'displayCategoryName'
                        ),
                        $W->LabelledWidget(
                            calendar_translate('Background color'),
                            $W->Select()
                                ->addOptions(
                                    array(
                                        'default' => calendar_translate('Site default'),
                                        'calendar' => calendar_translate('Calendar'),
                                        'category' => calendar_translate('Category'),
                                    )
                                )
                                ->setValue('default'),
                            'backgroundColor'
                        )
//                         $W->LabelledWidget(
//                             calendar_translate('Use event colors'),
//                             $W->CheckBox(),
//                             'slotEventOverlap'
//                         )
                    )->setVerticalSpacing(1, 'em')
                )->setFoldable(true, true)
            )
        );
    }


    /**
     *
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getRichIcon()
     */
    public function getRichIcon()
    {
        return $this->addon->getImagesPath() . 'calendar128.png';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getIcon()
     */
    public function getIcon()
    {
        return $this->addon->getImagesPath() . 'calendar48.png';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getThumbnail()
     */
    public function getThumbnail()
    {
        return $this->addon->getImagesPath() . 'thumbnail.png';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getConfigurationActions()
     */
    public function getConfigurationActions()
    {
        return array();
    }
}



class calendar_Portlet_Calendar extends Widget_Item implements portlet_PortletInterface
{

    private $id;

    /**
     *
     * @var Widget_FullCalendar
     */
    private $calendar;

    private $calendars;

    private $displayType;

    private $displayMode;

    private $weekMode;

    private $displayDetailedViewLink;


    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * {@inheritDoc}
     * @see Widget_Widget::getName()
     */
    public function getName()
    {
        return get_class($this);
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::getPortletDefinition()
     */
    public function getPortletDefinition()
    {
        return new calendar_PortletDefinition_Calendar();
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::setPreferences()
     */
    public function setPreferences(array $configuration)
    {
//         require_once dirname(__FILE__) . '/controller.class.php';
//         /* @var $fileManagerAction Widget_Action */

//         $W =  bab_Widgets();
//         $App = calendar_App();
//         $ctrl = $App->Controller();

//         $this->calendar = $W->FullCalendar();

        bab_Registry::sessionOverride('/calendar/' . $this->id . '/calendars', $configuration['calendars']);
        bab_Registry::sessionOverride('/calendar/' . $this->id . '/displayType', $configuration['displayType']);
        bab_Registry::sessionOverride('/calendar/' . $this->id . '/displayMode', isset($configuration['displayMode']) ? $configuration['displayMode'] : calendar_CtrlCalendar::LIST_MODE_DATE_CALENDAR);
        bab_Registry::sessionOverride('/calendar/' . $this->id . '/weekMode', isset($configuration['weekMode']) ? $configuration['weekMode'] : 'fixed');

        bab_Registry::sessionOverride('/calendar/' . $this->id . '/displayCalendars', isset($configuration['displayCalendars']) && $configuration['displayCalendars']);

        bab_Registry::sessionOverride('/calendar/' . $this->id . '/displayDetailedViewLink', isset($configuration['displayDetailedViewLink']) && $configuration['displayDetailedViewLink']);
        bab_Registry::sessionOverride('/calendar/' . $this->id . '/minTime', isset($configuration['minTime']) ? $configuration['minTime'] : '');
        bab_Registry::sessionOverride('/calendar/' . $this->id . '/maxTime', isset($configuration['maxTime']) ? $configuration['maxTime'] : '');
        bab_Registry::sessionOverride('/calendar/' . $this->id . '/slotDuration', isset($configuration['slotDuration']) ? $configuration['slotDuration'] : '00:30');

        bab_Registry::sessionOverride('/calendar/' . $this->id . '/visibleDays', isset($configuration['visibleDays']) ? $configuration['visibleDays'] : array(0 => true, 1 => true, 2 => true, 3 => true, 4 => true, 5 => true, 6 => true));

        bab_Registry::sessionOverride('/calendar/' . $this->id . '/slotEventOverlap', isset($configuration['slotEventOverlap']) ? $configuration['slotEventOverlap'] : false);

        bab_Registry::sessionOverride('/calendar/' . $this->id . '/backgroundColor', $configuration['backgroundColor']);
        bab_Registry::sessionOverride('/calendar/' . $this->id . '/displayCategoryName', $configuration['displayCategoryName']);

        bab_Registry::sessionOverride('/calendar/' . $this->id . '/displayHeaderBar', $configuration['displayHeaderBar']);

//         $availableCalendars = calendar_getAvailableCalendars();
//         $resources = array();
//         foreach ($this->calendars as $calendarId) {
//             list($type) = explode('/', $calendarId);
//             if (isset($availableCalendars[$type][$calendarId])) {

//                 $resource = array(
//                     'id' => $calendarId,
//                     'name' => '<div class="icon-left-symbolic icon-left icon-symbolic">' . $availableCalendars[$type][$calendarId]->getName() . '</div>'
//                 );
//                 $resources[] = $resource;
//             }
//         }
//         $this->calendar->setResources($resources);

//         $this->calendar->setFirstDayOfWeek(1);

//         $fetchPeriodsAction = $ctrl->Calendar()->events($this->calendars);
//         $fetchPeriodsAction->setParameter('backgroundColor', $configuration['backgroundColor']);
//         $fetchPeriodsAction->setParameter('displayCategoryName', $configuration['displayCategoryName']);
//         $this->calendar->fetchPeriods($fetchPeriodsAction);

//         $this->calendar->setView($this->displayType);

//         $minTime = isset($configuration['minTime']) ? $configuration['minTime'] : '';
//         if (!empty($minTime)) {
//             $this->calendar->setStartTime($minTime);
//         }

//         $maxTime = isset($configuration['maxTime']) ? $configuration['maxTime'] : '';
//         if (!empty($maxTime) && $maxTime != '00:00') {
//             $this->calendar->setEndTime($maxTime);
//         }


//         $slotDuration = isset($configuration['slotDuration']) ? $configuration['slotDuration'] : '00:30';
//         $this->calendar->setSlotDuration($slotDuration);

//         $visibleDays = isset($configuration['visibleDays']) ? $configuration['visibleDays'] : array(0 => true, 1 => true, 2 => true, 3 => true, 4 => true, 5 => true, 6 => true);
//         $hiddenDays = array();
//         foreach ($visibleDays as $day => $visible) {
//             if ($visible == 0) {
//                 $hiddenDays[] = $day;
//             }
//         }
//         $this->calendar->setHiddenDays($hiddenDays);

//         $slotEventOverlap = isset($configuration['slotEventOverlap']) ? (bool)$configuration['slotEventOverlap'] : false;
//         $this->calendar->setSlotEventOverlap($slotEventOverlap);

//         $leftButtons = array();
//         $centerButtons= array();
//         $rightButtons = array();

//         $displayHeaderBar = isset($configuration['displayHeaderBar']) ? $configuration['displayHeaderBar'] : true;
//         if ($displayHeaderBar) {
//             $leftButtons = isset($configuration['leftButtons']) ? $configuration['leftButtons'] : array(Widget_FullCalendar::VIEW_MONTH, Widget_FullCalendar::VIEW_WEEK);
//             $centerButtons = isset($configuration['centerButtons']) ? $configuration['centerButtons'] : array(Widget_FullCalendar::TITLE);
//             $rightButtons = isset($configuration['rightButtons']) ? $configuration['rightButtons'] : array(Widget_FullCalendar::BUTTON_PREVIOUS, Widget_FullCalendar::BUTTON_NEXT);
//         } else {
//             $this->calendar->addClass('calendar-synchronized');
//         }

//         if (isset($_SESSION['calendar']['fromDate'])) {
//             $tempDate = BAB_DateTime::fromIsoDateTime($_SESSION['calendar']['fromDate']);
//             $this->calendar->setDate($tempDate);
//         }

//         $this->calendar->setHeaderLeft(implode(',', $leftButtons));
//         $this->calendar->setHeaderCenter(implode(',', $centerButtons));
//         $this->calendar->setHeaderRight(implode(',', $rightButtons));

//         $this->calendar->onPeriodMoved($ctrl->Calendar()->moveEvent());
//         $this->calendar->setWeekMode($this->weekMode);

//         $addEventAction = $ctrl->Calendar()->addEvent();
//         $addEventAction->setParameter('calendars', $this->calendars);

//         $this->calendar->onDoubleClick($addEventAction, true);
    }


    /**
     * {@inheritDoc}
     * @see Widget_Frame::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $App = calendar_App();
        $addon = bab_getAddonInfosInstance('calendar');

        $ctrl = $App->Controller()->Calendar(false);



        $box = $ctrl->display($this->id);
        $box->setId('calendar_' . $this->id); // The widget item id.

        $display = $box->display($canvas);

        return $display;
    }


//     /**
//      * (non-PHPdoc)
//      * @see Widget_FullCalendar::display()
//      */
//     public function display(Widget_Canvas $canvas)
//     {
//         $W = bab_Widgets();
//         $App = calendar_App();
//         $ctrl = $App->Controller();
//         $_SESSION['calendar']['displayType'] = $this->displayType;

//         $registry = bab_getRegistryInstance();
//         $registry->changeDirectory("/calendar/$this->id/0/");

//         if (isset($this->calendars)) {
//             $portCal = $this->calendars;
//             if(!is_array($portCal)){
//                 $portCal = explode(',',$portCal);
//             }
//             sort($portCal);
//             $regCal = $registry->getValue('agendaOrder');
//             if(!is_array($regCal)){
//                 $regCal = explode(',',$regCal);
//             }
//             sort($regCal);

//             if ($registry->getValue('agendaOrder') == null || $portCal != $regCal) {
//                 $registry->setKeyValue('agendaOrder', $this->calendars);
//             }
//             if ($this->displayType == calendar_CtrlCalendar::TYPE_LIST) {
//                 $listEventsBox = $ctrl->eventListBox($this->displayMode, $this->id);
//                 $listEventsBox->setSizePolicy('calendar-detailed-view');

//                 $display = $W->VBoxItems(
//                     $listEventsBox
//                 )->setVerticalSpacing(2, 'em');
//                 return $display;
//             }
//         }

//         if (isset($this->displayCalendars)) {
//             $calendarBox = $W->VBoxItems();
//             $calendarBox->addClass(Func_Icons::ICON_LEFT_16);
//             $availableCalendars = calendar_getAvailableCalendars();
//             foreach ($this->calendars as $calendar) {
//                 list($type) = explode('/', $calendar);
//                 if (isset($availableCalendars[$type][$calendar])) {
//                     $calendarBox->addItem(
//                         $W->FlowItems(
//                             $W->FlowItems(
//                                 $W->Label(' ')
//                                     ->addClass('widget-counter-label')
//                                     ->setCanvasOptions(Widget_Item::options()->backgroundColor('#' . $availableCalendars[$type][$calendar]->getBgcolor())),
//                                 $W->Label($availableCalendars[$type][$calendar]->getName())
//                             )->setSizePolicy('widget-90pc')
//                             ->setHorizontalSpacing(1, 'em'),
//                             $W->CheckBox()->setSizePolicy('widget-10pc')
//                         )->setSizePolicy('widget-list-element')
//                     );
//                 }
//             }
//             $calendarBox->addItem(
//                 $W->Link(
//                     $App->translate('New calendar')
//                 )->setSizePolicy('widget-list-element')
//                 ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
//             );
//             $inDisplay = $W->VBoxItems(
//                 $this->calendar
//             );
//             if ($this->displayDetailedViewLink) {
//                 $agendas = calendar_getPortletUserAgendaOrder($this->id, bab_getUserId());
//                 $linkToView = $W->Link(
//                     calendar_translate('Open detailed view'),
//                     $ctrl->Calendar()->printEventListPerDayPerCalendar($agendas)
//                 )->setOpenMode(Widget_Link::OPEN_POPUP)
//                 ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_VIEW_CALENDAR_LIST)
//                 ->setSizePolicy(Func_Icons::ICON_LEFT_16);

//                 if ($this->displayMode == calendar_CtrlCalendar::LIST_MODE_DATE) {
//                     $from = bab_DateTime::fromIsoDateTime(date('Y-m-d') . ' 00:00:00');
//                     $to = $from->cloneDate();
//                     $to->add(31, BAB_DATETIME_DAY);
//                     if (isset($_SESSION['calendar']['fromDate']) && isset($_SESSION['calendar']['toDate'])) {
//                         $from = bab_DateTime::fromIsoDateTime($_SESSION['calendar']['fromDate']);
//                         $to = bab_DateTime::fromIsoDateTime($_SESSION['calendar']['toDate']);
//                     }
//                     $linkToView->setUrl($ctrl->Calendar()->printEventListPerDay($agendas, true)->url());
//                     $inDisplay->addItem($linkToView);
//                 }

//                 if ($this->displayMode == calendar_CtrlCalendar::LIST_MODE_DATE_CALENDAR) {
//                     $frame = $ctrl->Calendar(false)->displayAgendaButtons($this->id);
//                     $inDisplay->addItem($frame);
//                 }
//             }

//             $toolbar = $W->FlowItems()
//             ->addClass('widget-toolbar', Func_Icons::ICON_LEFT_16);

// //             $toolbar->addItem(
// //                 $W->Link(
// //                     $App->translate('Add event')
// //                 )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
// //             );

// //             $toolbar->addItem(
// //                 $W->Label(bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp())
// //             );

//             $toolbar->addItem(
//                 $W->Link(
//                     ''
//                 )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_ARROW_LEFT)
//             );

//             $toolbar->addItem(
//                 $W->Link(
//                     ''
//                 )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_ARROW_RIGHT)
//             );

//             $toolbar->addItem(
//                 $W->Label(bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp())
//             );

//             $toolbar->addItem(
//                 $W->Label(
//                     $App->translate('Octobre 2018')
//                 )->addClass('btn', 'widget-strong')
//             );

//             $configurationMenu= $W->Menu(
//                 null,
//                 $W->VBoxLayout()
//             );
//             $configurationMenu->setButtonClass('widget-actionbutton btn icon ' . Func_Icons::ACTIONS_CONTEXT_MENU);
//             $configurationMenu->setButtonLabel('');

//             $configurationMenu->addClass(Func_Icons::ICON_LEFT_16);

//             $searchForm = $W->Link(
//                 ''
//             )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_FIND);

//             $rightBox = $W->FlowItems();
//             $rightBox->addItem($searchForm);

//             $rightBox->addItem(
//                 $W->Label(bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp())
//             );


//             $rightBox->addItem(
//                 $W->Link(
//                     $App->translate('Day')
//                 )->addClass('widget-actionbutton') //, 'icon', Func_Icons::ACTIONS_VIEW_CALENDAR_DAY)
//             );

//             $rightBox->addItem(
//                 $W->Link(
//                     $App->translate('Week')
//                 )->addClass('widget-actionbutton') //, 'icon', Func_Icons::ACTIONS_VIEW_CALENDAR_WEEK)
//             );

//             $rightBox->addItem(
//                 $W->Link(
//                     $App->translate('Month')
//                 )->addClass('widget-actionbutton') //, 'icon', Func_Icons::ACTIONS_VIEW_CALENDAR_MONTH)
//             );

//             $rightBox->addItem(
//                 $W->Label(bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp())
//             );
//             $rightBox->addItem(
//                 $configurationMenu
//             );





//             $toolbar->addItem(
//                 $rightBox
//                 ->setSizePolicy('pull-right')
//             );

//             $display = $W->VBoxItems(
//                 $toolbar,
//                 $W->HBoxItems(
//                     $calendarBox->setSizePolicy('widget-25pc filemanager-side-panel-container'),
//                     $inDisplay->setSizePolicy('widget-75pc')
//                 )->setSizePolicy('widget-100pc')
//                 ->setVerticalAlign('top')
//                 ->addClass('widget-100pc')
//             )
//             ->display($canvas);
//         }

//         return $display;
//     }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::setPreference()
     */
    public function setPreference($name, $value)
    {

    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::setPortletId()
     */
    public function setPortletId($id)
    {
        $this->id = $id;
    }
}
