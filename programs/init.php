<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';



function calendar_upgrade($version_base, $version_ini)
{
    $addonName = 'calendar';
    $addon = bab_getAddonInfosInstance($addonName);

    $addonPhpPath = $addon->getPhpPath();

    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';

    require_once dirname(__FILE__) . '/calendar.php';

    $functionalities = new bab_functionalities();

    if ($functionalities->registerClass('Func_App_Calendar', $addonPhpPath . 'calendar.php')) {
        echo(bab_toHtml('Functionality "Func_App_Calendar" registered.'));
    }


    $App = calendar_App();


    $addon->removeAllEventListeners();

    $addonPhpPath = $addon->getPhpPath();

    $addon->addEventListener('bab_eventBeforePageCreated', 'calendar_onBeforePageCreated', 'init.php', -10);

    @bab_functionality::includefile('PortletBackend');

    if (class_exists('Func_PortletBackend')) {
        require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
        require_once dirname(__FILE__) . '/portletbackend.class.php';
        $functionalities = new bab_functionalities();
        $functionalities->registerClass('Func_PortletBackend_Calendar', $addonPhpPath . 'portletbackend.class.php');
    }

    @bab_functionality::includefile('WorkspaceAddon');
    if (class_exists('Func_WorkspaceAddon')) {
        $addonPhpPath = $addon->getPhpPath();
        $functionalities->registerClass('Func_WorkspaceAddon_Calendar', $addonPhpPath . 'workspaceaddon.class.php');
        echo(bab_toHtml('Functionality "Func_WorkspaceAddon_Calendar" registered.'));
    }

    return true;
}



function calendar_onBeforePageCreated(bab_eventBeforePageCreated $event)
{
    if ($icons = @bab_functionality::get('Icons')) {
        $icons->includeCss();
    }
//     $babBody = bab_getBody();
//     $addon = bab_getAddonInfosInstance('calendar');
//     $babBody->addStyleSheet($addon->getStylePath().'calendar.css');
}
