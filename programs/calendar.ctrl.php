<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2017 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/calendars.php';

/* @var $App Func_App_Calendar */
$App = bab_functionality::get('App/Calendar');


$App->includeController();




/**
 * This controller manages actions that can be performed on calendars.
 */
class calendar_CtrlCalendar extends calendar_Controller
{
    const TYPE_MONTH                = 'month';
    const TYPE_WEEK                 = 'week';
    const TYPE_WORKWEEK             = 'workweek';
    const TYPE_DAY                  = 'day';
    const TYPE_TIMELINE             = 'timeline';
    const TYPE_LIST                 = 'list';

    const LIST_MODE_DATE_CALENDAR   = 'listPerDatePerCalendar';
    const LIST_MODE_DATE            = 'listPerDate';






    public function display($portletId)
    {
        $W = bab_Widgets();
        $App = calendar_App();
        $Ui = $App->Ui();
        $ctrl = $this;

        $calendars = bab_Registry::get('/calendar/' . $portletId. '/calendars', array());

        $displayCalendars = bab_Registry::get('/calendar/' . $portletId. '/displayCalendars', true);

        $displayType = bab_Registry::get('/calendar/' . $portletId. '/displayType');
        $displayMode = bab_Registry::get('/calendar/' . $portletId. '/displayMode');
        $backgroundColor = bab_Registry::get('/calendar/' . $portletId. '/backgroundColor');
        $weekMode = bab_Registry::get('/calendar/' . $portletId. '/weekMode');
        $displayCategoryName = bab_Registry::get('/calendar/' . $portletId. '/displayCategoryName');
        $minTime = bab_Registry::get('/calendar/' . $portletId. '/minTime');
        $maxTime = bab_Registry::get('/calendar/' . $portletId. '/maxTime');
        $slotDuration = bab_Registry::get('/calendar/' . $portletId. '/slotDuration');
        $slotEventOverlap = bab_Registry::get('/calendar/' . $portletId. '/slotEventOverlap');
        $visibleDays = bab_Registry::get('/calendar/' . $portletId. '/visibleDays');
        $displayDetailedViewLink = bab_Registry::get('/calendar/' . $portletId. '/displayDetailedViewLink');
        $displayHeaderBar = bab_Registry::get('/calendar/' . $portletId. '/displayHeaderBar');

        $calendar = $W->FullCalendar();

        $availableCalendars = calendar_getAvailableCalendars();
        $resources = array();
        foreach ($calendars as $calendarId) {
            list($type) = explode('/', $calendarId);
            if (isset($availableCalendars[$type][$calendarId])) {

                $resource = array(
                    'id' => $calendarId,
                    'name' => '<div class="icon-left-symbolic icon-left icon-symbolic">' . $availableCalendars[$type][$calendarId]->getName() . '</div>'
                );
                $resources[] = $resource;
            }
        }
        $calendar->setResources($resources);

        $calendar->setFirstDayOfWeek(1);




        $fetchPeriodsAction = $ctrl->Calendar()->events($calendars);
        $fetchPeriodsAction->setParameter('backgroundColor', $backgroundColor);
        $fetchPeriodsAction->setParameter('displayCategoryName', $displayCategoryName);
        $calendar->fetchPeriods($fetchPeriodsAction);

        $calendar->setView($displayType);

        if (!empty($minTime)) {
            $calendar->setStartTime($minTime);
        }

        if (!empty($maxTime) && $maxTime != '00:00') {
            $calendar->setEndTime($maxTime);
        }

        $calendar->setSlotDuration($slotDuration);

        $hiddenDays = array();
        foreach ($visibleDays as $day => $visible) {
            if ($visible == 0) {
                $hiddenDays[] = $day;
            }
        }
        $calendar->setHiddenDays($hiddenDays);

        $calendar->setSlotEventOverlap($slotEventOverlap);

        $leftButtons = array();
        $centerButtons= array();
        $rightButtons = array();

        if ($displayHeaderBar) {
            $leftButtons = array(Widget_FullCalendar::VIEW_MONTH, Widget_FullCalendar::VIEW_WEEK); //
            $centerButtons = array(Widget_FullCalendar::TITLE); //
            $rightButtons = array(Widget_FullCalendar::BUTTON_PREVIOUS, Widget_FullCalendar::BUTTON_NEXT); //
//         } else {
//             $calendar->addClass('calendar-synchronized');
        }

        if (isset($_SESSION['calendar']['fromDate'])) {
            $tempDate = BAB_DateTime::fromIsoDateTime($_SESSION['calendar']['fromDate']);
            $calendar->setDate($tempDate);
        }

        $calendar->setHeaderLeft(implode(',', $leftButtons));
        $calendar->setHeaderCenter(implode(',', $centerButtons));
        $calendar->setHeaderRight(implode(',', $rightButtons));

        $calendar->onPeriodMoved($ctrl->Calendar()->moveEvent());
        $calendar->setWeekMode($weekMode);

        $addEventAction = $ctrl->Calendar()->addEvent();
        $addEventAction->setParameter('calendars', $calendars);

        $calendar->onDoubleClick($addEventAction, true);



        $_SESSION['calendar']['displayType'] = $displayType;




        $registry = bab_getRegistryInstance();
        $registry->changeDirectory("/calendar/$portletId/0/");

        if (isset($calendars)) {
            $portCal = $calendars;
            if(!is_array($portCal)){
                $portCal = explode(',',$portCal);
            }
            sort($portCal);
            $regCal = $registry->getValue('agendaOrder');
            if(!is_array($regCal)){
                $regCal = explode(',',$regCal);
            }
            sort($regCal);

            if ($registry->getValue('agendaOrder') == null || $portCal != $regCal) {
                $registry->setKeyValue('agendaOrder', $calendars);
            }
            if ($displayType == calendar_CtrlCalendar::TYPE_LIST) {
                $listEventsBox = $ctrl->eventListBox($displayMode, $portletId);
                $listEventsBox->setSizePolicy('calendar-detailed-view');

                $display = $W->VBoxItems(
                    $listEventsBox
                )->setVerticalSpacing(2, 'em');
                return $display;
            }
        }

        if (isset($displayCalendars)) {
            $calendarBox = $W->VBoxItems();
            $calendarBox->addClass(Func_Icons::ICON_LEFT_16);
            $availableCalendars = calendar_getAvailableCalendars();

            foreach ($calendars as $calendarId) {
                list($type) = explode('/', $calendarId);
                if (isset($availableCalendars[$type][$calendarId])) {
                    $calendarBox->addItem(
                        $W->FlowItems(
                            $W->FlowItems(
                                $W->Label(bab_nbsp() . bab_nbsp())
                                ->setCanvasOptions(Widget_Item::options()->backgroundColor('#' . $availableCalendars[$type][$calendarId]->getBgcolor())),
                                $W->Label($availableCalendars[$type][$calendarId]->getName())
                            )->setSizePolicy('widget-90pc')
                            ->setHorizontalSpacing(1, 'em'),
                        $W->CheckBox()->setSizePolicy('widget-10pc')
                        )->setSizePolicy('widget-list-element')
                    );
                }
            }
            $calendarBox->addItem(
                $W->Link(
                    $App->translate('New calendar')
                )->setSizePolicy('widget-list-element')
                ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
            );
            $inDisplay = $W->VBoxItems(
                $calendar
            );
            if ($displayDetailedViewLink) {
                $agendas = calendar_getPortletUserAgendaOrder($portletId, bab_getUserId());
                $linkToView = $W->Link(
                    calendar_translate('Open detailed view'),
                    $ctrl->Calendar()->printEventListPerDayPerCalendar($agendas)
                )->setOpenMode(Widget_Link::OPEN_POPUP)
                ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_VIEW_CALENDAR_LIST)
                ->setSizePolicy(Func_Icons::ICON_LEFT_16);

                if ($displayMode == calendar_CtrlCalendar::LIST_MODE_DATE) {
                    $from = bab_DateTime::fromIsoDateTime(date('Y-m-d') . ' 00:00:00');
                    $to = $from->cloneDate();
                    $to->add(31, BAB_DATETIME_DAY);
                    if (isset($_SESSION['calendar']['fromDate']) && isset($_SESSION['calendar']['toDate'])) {
                        $from = bab_DateTime::fromIsoDateTime($_SESSION['calendar']['fromDate']);
                        $to = bab_DateTime::fromIsoDateTime($_SESSION['calendar']['toDate']);
                    }
                    $linkToView->setUrl($ctrl->Calendar()->printEventListPerDay($agendas, true)->url());
                    $inDisplay->addItem($linkToView);
                }

                if ($displayMode == calendar_CtrlCalendar::LIST_MODE_DATE_CALENDAR) {
                    $frame = $ctrl->Calendar(false)->displayAgendaButtons($portletId);
                    $inDisplay->addItem($frame);
                }
            }

            $toolbar = $Ui->toolbar();

            $toolbar->addItem(
                $previousButton = $W->Label(
                    ''
                )->setIcon(Func_Icons::ACTIONS_ARROW_LEFT)
                ->addClass('widget-actionbutton')
            );
            $calendar->setPreviousButton($previousButton);

            $toolbar->addItem(
                $nextButton = $W->Label(
                    ''
                )->setIcon(Func_Icons::ACTIONS_ARROW_RIGHT)
                ->addClass('widget-actionbutton')
            );
            $calendar->setNextButton($nextButton);

            $toolbar->addItem(
                $W->Label(bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp())
            );

            $toolbar->addItem(
                $titleWidget = $W->Label(
                    '--'
                )->addClass('btn', 'widget-strong')
            );

            $calendar->setTitleWidget($titleWidget);

            $configurationMenu = $W->Menu(
                null,
                $W->VBoxLayout()
            );
            $configurationMenu->setButtonClass('widget-actionbutton btn icon ' . Func_Icons::ACTIONS_CONTEXT_MENU);
            $configurationMenu->setButtonLabel('');

            $configurationMenu->addClass(Func_Icons::ICON_LEFT_16);

            $searchForm = $W->Link(
                ''
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_FIND);

            $rightBox = $W->FlowItems();
            $rightBox->addItem($searchForm);

            $rightBox->addItem(
                $W->Label(bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp())
            );


            $rightBox->addItem(
                $W->Link(
                    $App->translate('Day'),
                    $this->proxy()->setView($portletId, Widget_FullCalendar::VIEW_DAY)
                )->addClass('widget-actionbutton') //, 'icon', Func_Icons::ACTIONS_VIEW_CALENDAR_DAY)
                ->setAjaxAction()
            );

            $rightBox->addItem(
                $W->Link(
                    $App->translate('Week'),
                    $this->proxy()->setView($portletId, Widget_FullCalendar::VIEW_WEEK)
                )->addClass('widget-actionbutton') //, 'icon', Func_Icons::ACTIONS_VIEW_CALENDAR_WEEK)
                ->setAjaxAction()
            );

            $rightBox->addItem(
                $W->Link(
                    $App->translate('Month'),
                    $this->proxy()->setView($portletId, Widget_FullCalendar::VIEW_MONTH)
                )->addClass('widget-actionbutton') //, 'icon', Func_Icons::ACTIONS_VIEW_CALENDAR_MONTH)
                ->setAjaxAction()
            );

            $rightBox->addItem(
                $W->Label(bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp())
            );
            $rightBox->addItem(
                $configurationMenu
            );


            $toolbar->addItem(
                $rightBox
                ->setSizePolicy('pull-right')
            );

            $box = $W->VBoxItems(
                $toolbar,
                $W->HBoxItems(
                    $calendarBox->setSizePolicy('widget-25pc filemanager-side-panel-container'),
                    $inDisplay->setSizePolicy('widget-75pc')
                )->setSizePolicy('widget-100pc')
                ->setVerticalAlign('top')
                ->addClass('widget-100pc')
            );

            $box->setReloadAction($this->proxy()->display($portletId));

            return $box;
        }
    }


    public function setView($portletId, $view)
    {
        bab_Registry::userOverride('/calendar/' . $portletId. '/displayType', $view);

        return true;
    }




    /**
     * Returns a json encoded array of events
     *
     * @param string    $inputDateFormat    The format of start and end parameters.
     * @param string    $start              Start date/time
     * @param string    $end                End date/time
     */
    public function events($calendars = null, $inputDateFormat = 'timestamp', $start = null, $end = null, $backgroundColor = 'default', $displayCategoryName = false)
    {
        if ($inputDateFormat === 'timestamp') {
            $startDate = BAB_DateTime::fromTimeStamp($start);
            $endDate = BAB_DateTime::fromTimeStamp($end);
        } else {
            $startDate = BAB_DateTime::fromIsoDateTime($start);
            $endDate = BAB_DateTime::fromIsoDateTime($end);
        }
        $tempDate = $startDate->cloneDate();

        // We ensure that if the displat type is month, is within the current month, as the first week diplayed may be from last month
        if (isset($_SESSION['calendar']['displayType']) && $_SESSION['calendar']['displayType'] == calendar_CtrlCalendar::TYPE_MONTH) {
            $tempDate->add(7, BAB_DATETIME_DAY);
        }

        //fromDate and toDate in session are used to display the events on the detailed view
        //We store in session the first day of the current month
        $dateDiff = BAB_DateTime::dateDiffIso($startDate->getIsoDateTime(), $endDate->getIsoDateTime());


        if ($dateDiff > 7) {
            //*** MONTH VIEW
            $currentYearMonth = substr($tempDate->getIsoDate(), 0, 7);
            while (substr($tempDate->getIsoDate(), 0, 7) >= $currentYearMonth) {
                $tempDate->add(-1, BAB_DATETIME_DAY);
            }
            $tempDate->add(1, BAB_DATETIME_DAY);
            $_SESSION['calendar']['fromDate'] = $tempDate->getIsoDateTime();

            //We store in session the last of the current month
            $tempEnd = $tempDate->cloneDate();

            while (substr($tempEnd->getIsoDate(), 0, 7) <= $currentYearMonth){
                $tempEnd->add(1, BAB_DATETIME_DAY);
            }
            $tempEnd->add(-1, BAB_DATETIME_DAY);
            $_SESSION['calendar']['toDate'] = $tempEnd->getIsoDate().' 23:59:59';
        } else {
            //*** WEEK VIEW OR DAY
            $_SESSION['calendar']['fromDate'] = $startDate->getIsoDateTime();
            $tempEnd = $endDate->cloneDate();
            $tempEnd->add(-1, BAB_DATETIME_DAY);
            $_SESSION['calendar']['toDate'] = $tempEnd->getIsoDate().' 23:59:59';
        }

        $jsonEvents = array();

        $allCalendars = bab_getICalendars()->getCalendars();

        require_once dirname(__FILE__) . '/calendars.php';
        foreach ($calendars as $calendarId) {
            $calendarEvents = calendar_getCalendarEvents($startDate, $endDate, array($calendarId));
            foreach ($calendarEvents as $calendarEvent) {
                $jsonEvents[] = calendar_Event::fromCalendarPeriod($calendarEvent, $allCalendars[$calendarId], $backgroundColor, $displayCategoryName);
            }
        }

        header('Content-type: application/json');
        $json = bab_json_encode($jsonEvents);
        $json = bab_convertStringFromDatabase($json, bab_charset::UTF_8);

        echo $json;
        die;
    }



    /**
     * Sets the date to be displayed next time a calendar view is displayed.
     *
     * @param string $date		ISO formatted date.
     *
     * @return calendar_Action
     */
    public function setDate($date = null)
    {
        if (isset($date) && is_string($date)) {
            $_SESSION['calendar']['dateToShow'] = $date;
        }

        $W = bab_Widgets();

        $from = bab_DateTime::fromIsoDateTime($date . ' 00:00:00');
        $to = $from->cloneDate();
        $to->add(1, BAB_DATETIME_DAY);
        $to->add(-1, BAB_DATETIME_SECOND);
        $eventList = $this->eventList($from, $to, $from);
        $eventList->addAttribute('data-inset', 'true');

        $htmlCanvas = $W->HtmlCanvas();

        $html = $eventList->display($htmlCanvas);

        echo bab_json_encode(array('content-secondary' => $html));
        die;
    }



    /**
     * Moves the event by the specified offset in days and/or minutes relatively to its current start.
     *
     * @param int	$event            The calendar + event id (calendarId:eventId, eg.: public/2:b8512ece-9cb9-4cee-97ea-59988cea389e)
     * @param int	$offsetDays
     * @param int	$offsetMinutes
     *
     * @return bool
     */
    public function moveEvent($event = null, $offsetDays = null, $offsetMinutes = null, $offsetDuration = null, $ajax = null)
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        require_once $GLOBALS['babInstallPath'] . '/utilit/calincl.php';
        require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';

        list($calendarId, $eventId) = explode(':', $event);

        $calendar = bab_getICalendars()->getEventCalendar($calendarId);

        if (!isset($calendar)) {
            die('Cannot instantiate the event calendar (' . $calendarId . ')');
        }

        $backend = $calendar->getBackend();

        $collection = $backend->CalendarEventCollection($calendar);

        $period = $backend->getPeriod($collection, $eventId);

        if (!isset($period)) {
            die('Cannot instantiate the event (' . $eventId . ')');
        }

        $start = bab_DateTime::fromICal($period->getProperty('DTSTART'));
        $end = bab_DateTime::fromICal($period->getProperty('DTEND'));

        if (isset($offsetDays)) {
            $start->add($offsetDays, BAB_DATETIME_DAY);
            $end->add($offsetDays, BAB_DATETIME_DAY);
        }
        if (isset($offsetMinutes)) {
            $start->add($offsetMinutes, BAB_DATETIME_MINUTE);
            $end->add($offsetMinutes, BAB_DATETIME_MINUTE);
        }
        if (isset($offsetDuration)) {
            $end->add($offsetDuration, BAB_DATETIME_MINUTE);
        }

        $period->setProperty('DTSTART', $start->getICal());
        $period->setProperty('DTEND', $end->getICal());


        list($calendarType,) = explode('/', $calendarId);
        $bfree = ($calendarType == 'personal');

        $eventArgs = array(
            'calid' => $calendarId,
            'evtid' => $eventId,
            'selected_calendars' => array($calendarId),
            'title' => $period->getProperty('SUMMARY'),
            'description' => $period->getProperty('DESCRIPTION'),
            'descriptionformat' => 'html',
            'location' => $period->getProperty('LOCATION'),
            'yearbegin' => $start->getYear(),
            'monthbegin' => $start->getMonth(),
            'daybegin' => $start->getDayOfMonth(),
            'timebegin' => $start->getHour() . ':' . $start->getMinute(),
            'yearend' => $end->getYear(),
            'monthend' => $end->getMonth(),
            'dayend' => $end->getDayOfMonth(),
            'timeend' => $end->getHour() . ':' . $end->getMinute(),
            'owner' => bab_getUserId(),
            'bfree' => $bfree
        );

        $calendarIds = array($calendarId);
        $errorMessages = null;
        $result = bab_saveEvent($calendarIds, $eventArgs, $errorMessages, BAB_CAL_EVT_CURRENT);

        if (!$result) {
            if (!isset($errorMessages)) {
                $errorMessages = calendar_translate('The period is not available.');
            }
            $this->addError($errorMessages);
        }

        return true;
    }



    /**
     * Displays a form to edit a new calendar event.
     *
     * @param string	$date		ISO formatted date 'YYYY-MM-DD'
     * @param string	$time		ISO formatted time 'HH:MM'
     * @param string	$dateEnd	ISO formatted date 'YYYY-MM-DD'
     * @param string	$timeEnd	ISO formatted time 'HH:MM'
     * @param string	$title
     * @param string	$location
     * @param string	$description
     *
     * @return Widget_Action
     */
    public function addEvent($date = null, $time = null, $dateEnd = null, $timeEnd = null, $title = null, $location = null, $description = null, $calendar = null, $error = null, $calendars = null)
    {
        require_once dirname(__FILE__) . '/calendar.ui.php';

        $W = bab_Widgets();
        $page = $W->babPage();
        $page->setTitle(calendar_translate('New event'));


        $availableCalendars = calendar_getAvailableCalendars();
        $selectedCalendars = array();
        foreach ($calendars as $calendarId) {
            list($type) = explode('/', $calendarId);
            if (isset($availableCalendars[$type][$calendarId])) {
                $selectedCalendars[$calendarId] = $availableCalendars[$type][$calendarId]->getName();
            }
        }

        $editor = calendar_eventEditor($selectedCalendars, true);
        if (isset($date)) {
            $editor->setValue(array('event', 'startdate'), $date);
        }
        if (isset($dateEnd)) {
            $editor->setValue(array('event', 'enddate'), $dateEnd);
        }
        if (isset($time)) {
            $editor->setValue(array('event', 'starttime'), $time);
            list($hours, $minutes) = explode(':', $time);
            if (!isset($timeEnd)) {
                $hours += 2;
                if ($hours >= 24) {
                    $hours = 23;
                    $minutes = 59;
                }
                $timeEnd = sprintf('%02d:%02d', $hours, $minutes);
            }
            $editor->setValue(array('event', 'endtime'), $timeEnd);
        }
        if (isset($title)) {
            $editor->setValue(array('event', 'title'), $title);
        }
        if (isset($location)) {
            $editor->setValue(array('event', 'location'), $location);
        }
        if (isset($description)) {
            $editor->setValue(array('event', 'description'), $description);
        }
        if (isset($calendar)) {
            $editor->setValue(array('event', 'calendar'), $calendar);
        }

        if (isset($error)) {
            $page->addError($error);
        }

        $page->addItem($editor);
        return $page;
    }

    public function updatePeriodLink()
    {
        //var_dump(123);
    }



    /**
     * Displays a form to edit an existing calendar event.
     *
     * @param string    $event  The event id
     * @return Widget_Page
     */
    public function editEvent($event = null)
    {
        require_once dirname(__FILE__) . '/calendar.ui.php';
        require_once  $GLOBALS['babInstallPath'] . '/utilit/cal.periodcollection.class.php';

        $W = bab_Widgets();
        if(is_array($event)){
            $event = $event['event'];
            $calendarId = $event['calendar'];
            $eventId = null;
        }
        else{
            list($calendarId, $eventId) = explode(':', $event);
        }

        $calendar = bab_getICalendars()->getEventCalendar($calendarId);

        $selectedCalendars = array(
            $calendarId => $calendar->getName()
        );

        if (!isset($calendar)) {
            die('Cannot instantiate the event calendar (' . $calendarId . ')');
        }

        $collection = new bab_CalendarEventCollection;
        $collection->setCalendar($calendar);

        $backend = $calendar->getBackend();

        $period = $backend->getPeriod($collection, $eventId);

        if (!is_array($event) && !isset($period)) {
            die('Cannot instantiate the event (' . $eventId . ')');
        }


        $page = $W->babPage();

        $page->setTitle(calendar_translate('Edit event'));


        $editor = calendar_eventEditor($selectedCalendars, false);

        if(is_array($event)){
            $category = bab_getCalendarCategory($event['category']);
            if (isset($category)) {
                $category = $category['id'];
            }
            $editor->setValue(array('event', 'title'), $event['title']);
            $editor->setValue(array('event', 'description'), $event['description']);
            $editor->setValue(array('event', 'location'), $event['location']);
            $editor->setValue(array('event', 'startdate'), $event['startdate']);
            $editor->setValue(array('event', 'starttime'), $event['starttime']);
            $editor->setValue(array('event', 'endtime'), $event['endtime']);
            $editor->setValue(array('event', 'enddate'), $event['enddate']);
            $editor->setValue(array('event', 'category'), $category);
            $editor->setValue(array('event', 'calendar'), $calendarId);
        }
        else{
            $start = bab_DateTime::fromICal($period->getProperty('DTSTART'));
            $end = bab_DateTime::fromICal($period->getProperty('DTEND'));

            $category = bab_getCalendarCategory($period->getProperty('CATEGORIES'));
            if (isset($category)) {
                $category = $category['id'];
            }

            $startIsoTime = $start->getIsoTime();
            $endIsoTime = $end->getIsoTime();
            $startIsoTime = substr($startIsoTime, 0, 5);
            $endIsoTime = substr($endIsoTime, 0, 5);
            $data = $period->getData();
            $editor->setValue(array('event', 'title'), $period->getProperty('SUMMARY'));
            $editor->setValue(array('event', 'description'), $data['description']);
            $editor->setValue(array('event', 'location'), $period->getProperty('LOCATION'));
            $editor->setValue(array('event', 'startdate'), $start->getIsoDate());
            $editor->setValue(array('event', 'starttime'), $startIsoTime);
            $editor->setValue(array('event', 'endtime'), $endIsoTime);
            $editor->setValue(array('event', 'enddate'), $end->getIsoDate());
            $editor->setValue(array('event', 'category'), $category);
            $editor->setValue(array('event', 'calendar'), $calendarId);
            $editor->setHiddenValue('event[id]', $event);
        }

        //Check other calendars for this event
        global $babDB;
        $eventMysqliResult = $babDB->db_query("SELECT * FROM ".BAB_CAL_EVENTS_TBL." WHERE uuid=".$babDB->quote($eventId));
        $eventResult = $babDB->db_fetch_assoc($eventMysqliResult);

        $eventOwnerMysqliResult = $babDB->db_query("SELECT * FROM ".BAB_CAL_EVENTS_OWNERS_TBL." WHERE id_event=".$babDB->quote($eventResult['id']));
        if($babDB->db_num_rows($eventOwnerMysqliResult) > 1){
            $page->addItem(
                $W->Title(
                    calendar_translate('This event exists on others calendars'),
                    4
                )
            );
        }

        $page->addItem($editor);

        return $page;
    }


    /**
     * Saves the event and returns to the previous page.
     *
     * @param array $event
     * @return Widget_Action
     */
    public function saveEvent($event = null)
    {
        if (!is_array($event)) {
            return true;
        }
        require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

        $event['startDatetime'] = $event['startdate'] . ' ' .  $event['starttime'];
        if ($event['enddate'] == '') {
            $event['enddate'] =  $event['startdate'];
        }
        $event['endDatetime'] = $event['enddate'] . ' ' .  $event['endtime'];


        $dateStart = BAB_DateTime::fromIsoDateTime($event['startDatetime'].':00');
        $dateEnd = BAB_DateTime::fromIsoDateTime($event['endDatetime'].':00');

        if (BAB_DateTime::compare($dateStart, $dateEnd) == 1) {
            $event['startDatetime'] = $event['enddate'] . ' ' .  $event['endtime'];
            $event['endDatetime'] = $event['startdate'] . ' ' .  $event['starttime'];
        } elseif (BAB_DateTime::compare($dateStart, $dateEnd) == 0) {
            $this->addError(calendar_translate('The event must include at last one minute.'));
            return $this->editEvent($_POST);
        }
        $event['creatorId'] = bab_getUserId();


        if (isset($event['id'])) {
            list($calendarId, $eventId) = explode(':', $event['id']);
            $eventArgs = array(
                'calid' => $calendarId,
                'evtid' => $eventId,
                'selected_calendars' => array($calendarId),
            );
        } elseif (isset($event['calendar'])) {
            $calendarId = $event['calendar'];
            $eventArgs = array(
                'selected_calendars' => array($calendarId)
            );

        } else {
            /// ERROR
        }
        list($calendarType,) = explode('/', $calendarId);
        $bfree = ($calendarType == 'personal');
        $eventArgs += array(
            'title' => $event['title'],
            'description' => $event['description'],
            'descriptionformat' => 'html',
            'location' => $event['location'],
            'yearbegin' => $dateStart->getYear(),
            'monthbegin' => $dateStart->getMonth(),
            'daybegin' => $dateStart->getDayOfMonth(),
            'timebegin' => sprintf('%02d', $dateStart->getHour()) . ':' . sprintf('%02d', $dateStart->getMinute()),
            'yearend' => $dateEnd->getYear(),
            'monthend' => $dateEnd->getMonth(),
            'dayend' => $dateEnd->getDayOfMonth(),
            'timeend' => sprintf('%02d', $dateEnd->getHour()) . ':' . sprintf('%02d', $dateEnd->getMinute()),
            'owner' => bab_getUserId(),
            'category' => $event['category'],
            'groupe-notif' => isset($event['notify']) ? $event['notify'] : null,
            'bfree' => $bfree
        );

        $eventArgs['repeat_cb'] = false;

        if (isset($event['rule']['repeat_frequency'])) {

            switch ($event['rule']['repeat_frequency']) {
                case 'daily':
                    $eventArgs['repeat_cb'] = true;
                    $eventArgs['repeat'] = BAB_CAL_RECUR_DAILY;
                    $eventArgs['repeat_n_1'] = (int)$event['rule']['repeat_n_d'];
                    break;
                case 'weekly':
                    $eventArgs['repeat_cb'] = true;
                    $eventArgs['repeat'] = BAB_CAL_RECUR_WEEKLY;
                    $eventArgs['repeat_n_2'] = (int)$event['rule']['repeat_n_w'];
                    $eventArgs['repeat_wd'] = $event['rule']['repeat_wd'];
                    break;
                case 'monthly':
                    $eventArgs['repeat_cb'] = true;
                    $eventArgs['repeat'] = BAB_CAL_RECUR_MONTHLY;
                    $eventArgs['repeat_n_3'] = (int)$event['rule']['repeat_n_m'];
                    break;
                case 'yearly':
                    $eventArgs['repeat_cb'] = true;
                    $eventArgs['repeat'] = BAB_CAL_RECUR_YEARLY;
                    $eventArgs['repeat_n_4'] = (int)$event['rule']['repeat_n_y'];
                    break;
                case 'once':
                default:
                    break;
            }

            $dateRepeatEnd = BAB_DateTime::fromIsoDateTime($event['rule']['repeat_end_date'] . ' 00:00:00');

            $eventArgs['repeat_yearend'] = (int) $dateRepeatEnd->getYear();
            $eventArgs['repeat_monthend'] = (int) $dateRepeatEnd->getMonth();
            $eventArgs['repeat_dayend'] = (int) $dateRepeatEnd->getDayOfMonth();
        }


        $calendarIds = array($calendarId);
        $errorMessages = null;

        if (isset($event['id'])) {
            //Check other calendars for this event
            global $babDB;
            $eventUuid = explode(':', $event['id']);
            $eventMysqliResult = $babDB->db_query("SELECT * FROM ".BAB_CAL_EVENTS_TBL." WHERE uuid=".$babDB->quote($eventUuid[1]));
            $eventResult = $babDB->db_fetch_assoc($eventMysqliResult);

            $eventOwnerMysqliResult = $babDB->db_query("SELECT * FROM ".BAB_CAL_EVENTS_OWNERS_TBL." WHERE id_event=".$babDB->quote($eventResult['id']));
            while($eventOwnerResult = $babDB->db_fetch_assoc($eventOwnerMysqliResult)){
                $calendarIds[] = $eventOwnerResult['caltype'].'/'.$eventOwnerResult['id_cal'];
            }
            $result = bab_saveEvent($calendarIds, $eventArgs, $errorMessages, BAB_CAL_EVT_CURRENT);
        } else {
            $result = bab_saveEvent($calendarIds, $eventArgs, $errorMessages);
        }

        if (!$result) {
            if (!isset($errorMessages)) {
                $errorMessages = calendar_translate('The reservation is in conflict with other reservations.');
            }
           $this->addError($errorMessages);
        }

//         $this->addReloadSelector('.depends-' . $calendarId);
        $this->addReloadSelector('.depends-calendar-events');

        return true;
    }



    /**
     * Deletes the specified event.
     *
     * @param array	$event
     * @return boolean
     */
    public function deleteEvent($event = null)
    {
        if (!isset($event['id'])) {
            return true;
        }

        list($calendarId, $eventId) = explode(':', $event['id']);

        $calendar = bab_getICalendars()->getEventCalendar($calendarId);

        if (!isset($calendar)) {
            die('Cannot instantiate the event calendar (' . $calendarId . ')');
        }

        $backend = $calendar->getBackend();

        $collection = $backend->CalendarEventCollection($calendar);

        $period = $backend->getPeriod($collection, $eventId);

        $backend->deletePeriod($period);

        return true;
    }

    public function eventListBox($mode = calendar_CtrlCalendar::LIST_MODE_DATE_CALENDAR, $portletId = null)
    {
        require_once dirname(__FILE__) . '/calendar.ui.php';
        $agendas = calendar_getPortletUserAgendaOrder($portletId, bab_getUserId());
        $W = bab_Widgets();
        $linkToPrint = $W->Link(
            calendar_translate('Print')
        )
        ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_PRINT)
        ->setSizePolicy(Func_Icons::ICON_LEFT_16.' calendar-button-print');
        $buttonBox = $W->HBoxItems();
        switch ($mode){
            case calendar_CtrlCalendar::LIST_MODE_DATE:
                $ui = calendar_eventListPerDay($agendas);
                $linkToPrint->setUrl($this->proxy()->printEventListPerDay($agendas)->url());
                break;
            default:
                $ui = calendar_eventListPerDayPerCalendar($agendas);
                $link = $W->Link(
                    calendar_translate('Reorder agendas'),
                    $this->proxy()->editAgendaListOrder($portletId, $agendas)
                )
                ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_VIEW_LIST_TREE)
                ->setSizePolicy(Func_Icons::ICON_LEFT_16);
                $buttonBox->addItem($link);
                $linkToPrint->setUrl($this->proxy()->printEventListPerDayPerCalendar($agendas)->url());
                break;
        }
        $buttonBox->addItem($linkToPrint);
        $ui->addItem($buttonBox,0);
        $ui->setReloadAction($this->proxy()->eventListBox($mode, $portletId));
        return $ui;
    }

    public function printEventListPerDay($agendas, $popup = false, $maskPastEvents = false){
        require_once dirname(__FILE__) . '/calendar.ui.php';
        if($maskPastEvents){
            $pastEventsButtonText = calendar_translate('Display past events');
            $pastEventsButtonIcon = Func_Icons::ACTIONS_LIST_ADD;
        }
        else{
            $pastEventsButtonText = calendar_translate('Mask past events');
            $pastEventsButtonIcon = Func_Icons::ACTIONS_LIST_REMOVE;
        }
        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addItem(
            $W->HBoxItems(
                $W->Link(
                    calendar_translate('Print'),
                    '#'
                )->addAttribute("onclick", "window.print(); return false;")
                ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_PRINT)
                ->setSizePolicy(Func_Icons::ICON_LEFT_24),
                $W->Link(
                    $pastEventsButtonText,
                    $this->proxy()->printEventListPerDay($agendas, $popup, !$maskPastEvents)->url()
                )->addClass('widget-actionbutton', 'icon', $pastEventsButtonIcon)
                ->setSizePolicy(Func_Icons::ICON_LEFT_24)
            )->addClass('rm-printlink '.Func_Icons::ICON_LEFT_24)
        );
        $page->addItem(calendar_eventListPerDay($agendas, $popup, $maskPastEvents)->setSizePolicy('calendar-detailed-view calendar-detailed-view-print'));
        $page->setEmbedded(false);

        $addon = bab_getAddonInfosInstance('calendar');
        $page->addStyleSheet($addon->getStylePath().'calendar.css');
        return $page;
    }

    public function printEventListPerDayPerCalendar($agendas, $popup = false, $maskPastEvents = false){
        require_once dirname(__FILE__) . '/calendar.ui.php';
        if($maskPastEvents){
            $pastEventsButtonText = calendar_translate('Display past events');
            $pastEventsButtonIcon = Func_Icons::ACTIONS_LIST_ADD;
        }
        else{
            $pastEventsButtonText = calendar_translate('Mask past events');
            $pastEventsButtonIcon = Func_Icons::ACTIONS_LIST_REMOVE;
        }
//         var_dump($_SESSION['calendar']);
        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addItem(
            $W->HBoxItems(
                $W->Link(
                    calendar_translate('Print'),
                    '#'
                )->addAttribute("onclick", "window.print(); return false;")
                ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_PRINT)
                ->setSizePolicy(Func_Icons::ICON_LEFT_24),
                $W->Link(
                    $pastEventsButtonText,
                    $this->proxy()->printEventListPerDayPerCalendar($agendas, $popup, !$maskPastEvents)->url()
                )->addClass('widget-actionbutton', 'icon', $pastEventsButtonIcon)
                ->setSizePolicy(Func_Icons::ICON_LEFT_24)
            )->addClass('rm-printlink '.Func_Icons::ICON_LEFT_24)
        );
        $page->addItem(calendar_eventListPerDayPerCalendar($agendas, $popup, $maskPastEvents)->setSizePolicy('calendar-detailed-view calendar-detailed-view-print'));
        $page->setEmbedded(false);

        $addon = bab_getAddonInfosInstance('calendar');
        $page->addStyleSheet($addon->getStylePath().'calendar.css');
        return $page;
    }

    public function displayAgendaButtons($portletId, $itemId = null)
    {
        $W = bab_Widgets();

        $agendas = calendar_getPortletUserAgendaOrder($portletId, bab_getUserId());

        $linkToView = $W->Link(
            calendar_translate('Open detailed view'),
            $this->proxy()->printEventListPerDayPerCalendar($agendas, true)
        )->setOpenMode(Widget_Link::OPEN_POPUP)
        ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_VIEW_CALENDAR_LIST)
        ->setSizePolicy(Func_Icons::ICON_LEFT_16);

        $reorderLink = $W->Link(
            calendar_translate('Reorder agendas'),
            $this->proxy()->editAgendaListOrder($portletId, $agendas)
        )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
        ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_VIEW_LIST_TREE)
        ->setSizePolicy(Func_Icons::ICON_LEFT_16);

        $frame = $W->FlowItems();
        if(isset($itemId)){
            $frame->setId($itemId);
        }
        $frame->addItem($linkToView);
        $frame->addItem($reorderLink);
        $frame->setReloadAction($this->proxy()->displayAgendaButtons($portletId, $frame->getId()));
        return $frame;
    }

    public function editAgendaListOrder($portletId, $agendas)
    {
        require_once dirname(__FILE__) . '/calendar.ui.php';
        return calendar_editAgendaListOrder($portletId, $agendas);
    }

    public function saveAgendaListOrder($agendaOrder = null, $portletId = null)
    {
        if(isset($agendaOrder) && isset($portletId)){
            calendar_setPortletUserAgendaOrder($portletId, bab_getUserId(), $agendaOrder);
            return true;
        }

        return false;
    }

    public function saveGlobalAgendaListOrder($agendaOrder = null, $portletId = null)
    {
        if(isset($agendaOrder) && isset($portletId)){
            calendar_setPortletUserAgendaOrder($portletId, 0, $agendaOrder);
            return true;
        }

        return false;
    }

    public function useDefaultAgendaListOrder($portletId = null)
    {
        if(isset($portletId)){
            calendar_deletePortletUserAgendaOrder($portletId, bab_getUserId());
            return true;
        }

        return false;
    }
}
