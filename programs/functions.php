<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
bab_Functionality::includefile('Icons');



/**
 * This exception is thrown when the system tries to perform
 * an operation on an object that the user is not allowed to.
 */
class calendar_AccessException extends Exception
{
}


/**
 * Instanciates the App factory.
 *
 * @return Func_App_Calendar
 */
function calendar_App()
{
    return bab_Functionality::get('App/Calendar');
}


/**
 * Translates the string.
 *
 * @param string $str
 * @return string
 */
function calendar_translate($str, $str_plurals = null, $number = null)
{
    if ($translate = bab_functionality::get('Translate/Gettext')) {
        /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('calendar');
        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}



/**
 * Translates all the string in an array and returns a new array.
 *
 * @param array $arr
 * @return array
 */
function calendar_translateArray($arr)
{
	$newarr = $arr;

	foreach ($newarr as &$str) {
		$str = calendar_translate($str);
	}
	return $newarr;
}




/**
 * Redirects to the specified url or action.
 * This function makes the current script die.
 *
 * @param Widget_Action|string $url
 */
function calendar_redirect($url)
{
	global $babBody;

	if ($url instanceof Widget_Action) {
		$url = $url->url();
	}
	if (!empty($babBody->msgerror)) {
		$url .=  '&msgerror=' . urlencode($babBody->msgerror);
	}

	header('Location: ' . $url);
	die;
}
